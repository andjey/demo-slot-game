
The Game
========

Rules
-----

This is a simple slot game of chance. Just choose fruit and spin the wheel. If fruits match then you
 win a score. Can choose another fruit and repeat until full satisfaction. 

Created once upon a time 
 for demo purposes only, possible need to some improvements and fixing.


Features
--------

 * Totally without dependencies just native JavaScript
 * Decoupled structure (with namespaces)
 * Custom Advanced Module Definition (AMD)
 * Nothing is stored in the global scope

 * Model with dot-notation
 * Flow-control
 * Event Emitter
 * Finite State Machine
 * Browser detection
 * Simple DOM interface
 * DOM events

 * Asset manager with resource loaders:
 * Sprites and texts.. etc
 * Scene with layers
 * Animated scene components
 * Some canvas rendering optimizations

 * Catch errors
 * Server to handle static and client errors/log messages
 * Few useful utilities, helpers and polyfills


Use locally
-----------

1. Node.js (just native modules)

    node server.js

2. Chrome will throw Cross Origin error. To prevent this just use the flag `--allow-file-access-from-files`

 * (osx) `open -a 'Google Chrome' --args -allow-file-access-from-files`
 * (unix) `google-chrome  --allow-file-access-from-files`
 * (win)  `C:\ ... \Application\chrome.exe --allow-file-access-from-files`
 * then drag'n'drop `index.html` to the browser window

