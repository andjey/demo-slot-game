/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Core:
 *
 *  * AMD
 *  * AJAX Loader
 *  * Flow control
 *  * Catch errors
 *
 */

( function() {
    'use strict';


    // AMD
    // --------------------------------------------------------------------------------------------------------------

	// TODO: refactor to the event-driven amd behaviour can make the source more simple

    var
        // locals
        modules = {},   // modules registry
        loaded = {},    // cache of loaded scripts
        waits = {},     // script onload functions


        // params
        scripts = document.getElementsByTagName( 'script' ),
        current = scripts[ scripts.length - 1 ],

        // Config

        // module extension
        extension = '.js',

        // script to run after init
        firstAttr = 'data-run',
        first = current && current.getAttribute( firstAttr ) || false,

        // module root path
        baseurlAttr = 'data-base',
        baseUrl = current && current.getAttribute( baseurlAttr ) || '',
        baseUrl = baseUrl && baseUrl + '/';

    /**
     * Load 'n run the first-run script when AMD and AJAX will be fully loaded
     */
    function init() {

        if ( first )
            load( baseUrl + first,
                function( err, res ) {
                    if ( !err )
                        include( res.response );
                });
    }

    /**
     * AMD `define`
     * Module scope mixed with:
     *
     *  * define()
     *  * require()
     *  * ajax()
     *  * load()
     *  * async()
     *  * sync()
     *
     * @param {string} name
     * @param {string|string[]} [deps]
     * @param {function} factory
     */
    function define( name, deps, factory, callback ) {

        // args
        // optional
        if ( 'string' != typeof name )
            callback = factory,
            factory = deps,
            deps = name,
            name = undefined;
        if ( 'function' == typeof deps )
            callback = factory,
            factory = deps,
            deps = [];

        // checking
        if ( modules.hasOwnProperty( name ))
            throw new Error( 'Module already defined: ' + name );

        // first, store as loaded
        if ( name )
            loaded[ name ] = factory;

        // load dependencies
        require( deps, function() {

            // clear cache
            delete loaded[ name ];
            // init factory function
            var mod = factory.apply( null, arguments );
            if ( name ) modules[ name ] = mod;

            // system callback:
            // to allow use of multiple dependencies
            callback && callback( null, {
                name: name,
                module: mod
            });
        });
    }

    /**
     * AMD `require`
     * @param {string|string[]} names - module name or array of names, like [ 'mod1', ... 'modN' ]
     * @param {function} callback - call with objects of loaded modules, like ( mod1, ... modN )
     */
    function require( names, callback ) {
        names = names ? [].concat( names ) : false;
        callback = 'function' == typeof callback ? callback : false;

        // checking
        if ( !names )
            throw new Error( 'Nothing to require' );

        // action
        async(
            // async load of each dep-module
            names.map( function( name ) {
                return function( next ) {
                    requireOne( name, next );
                };
            }),
            // results
            function( err, res ) {
                if ( err ) throw err;
                callback && callback.apply( null, res );
            }
        );
    }

    /**
     * Logic of `require`: prepare, load and init script
     * @param {string} path
     * @param {function} callback
     */
    function requireOne( path, callback ) {
        var
            // script url
            url = baseUrl + path + extension,
            // get last word from path
            name = path && ( path.match( /([^\/]+)$/g ) || [] ).shift();

        // action
        if ( !name )
            throw new Error( 'Bad module name in path: ' + path );

        // callback exists module
        if ( modules.hasOwnProperty( name ))
            callback( null, modules[ name ]);

        // load if not loaded
        else if ( !loaded.hasOwnProperty( name )) {

            // mark as loading
            loaded[ name ] = null;
            waits[ name ] = [];

            // load script
            load( url,
                function( err, res ) {

                    // results
                    if ( err )
                        return callback( new Error( 'Error load module: ' + name ));
                    execute( res.response );
                }
            );
        }
        // need to wait until the module will be loaded
        else waits[ name ].push( callback );

        function execute( code ) {
            // execute loaded code
            include( code, name, function( err, mod ) {

                // callback module object
                if ( err ) throw err;
                else callback( null, mod );

                // if ( mod ) callback( null, mod );
                // TODO: else callback( new Error( 'Module is not AMD: ' + name ));

                // call waiting functions
                ( waits[ name ] || [] ).forEach( function( fn ) {
                    fn( null, mod );
                });
                // clear waiting queue
                delete waits[ name ];
            });
        }
    }

    /**
     * Run loaded script code
     * Code will run in the global context with scope filled by `api` content
     * @param {string} code
     * @param name
     * @param callback
     */
    function include( code, name, callback ) {
        var
            // custom scope
            scope = {
                // amd
                define: define,
                require: require,
                // ajax
                ajax: ajax,
                load: load,
                // flow
                async: async,
                sync: sync
            },
            // string of argument names
            fn = Function( Object.keys( scope ).join( ',' ), code );

        // override `define`
        // ( to allow multiple sub-deps )
        if ( callback )
            scope.define = function() {

                // call origin define function
                define.apply( null,
                    [].slice.call( arguments ).concat(
                        // callback
                        function( err, res ) {
                            // check is same module loaded
                            // or first `define` without the name
                            if ( name == res.name
                                || undefined === res.name )
                                callback( err, res.module );
                        }
                    )
                );
            };

        // run loaded script
        var args = Object.keys( scope ).map( function( v ) { return scope[ v ]; });
        fn.apply( null, args );
    }


    // AJAX
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Cross browser XHR creation
     * @returns {XMLHttpRequest|ActiveXObject} - xhr
     */
    function request() {
        var req;
        // most of browsers
        if ( window.XMLHttpRequest )
            req = new XMLHttpRequest();

        // IE implementation
        else if ( window.ActiveXObject ) {
            // different IE versions
            try {
                req = new ActiveXObject( 'Msxml2.XMLHTTP' );
            } catch ( err ){}
            try {
                req = new ActiveXObject( 'Microsoft.XMLHTTP' );
            } catch ( err ){}
        }
        return req;
    }

    /**
     * Ajax Query
     * ----------
     * Complete interface for AJAX queries
     *
     * @param {object} params
     * @returns {Error|XMLHttpRequest|ActiveXObject} - error or xhr object
     */
    function ajax( params ) {
        var
            // params
            params = params || {},
            url = params.url,
            asyncMode = params.sync ? false : true,     // default: true
            method = params.method && params.method.toUpperCase(),
            timeout = params.timeout || 30000,
            ontimeout = 'function' == typeof params.ontimeout ? params.ontimeout : false,
            cache = undefined !== params.cache ? !!params.cache : true,
            callback = 'function' == typeof params.callback ? params.callback : false,
            data = params.data instanceof  Object ? Object.keys( params.data ).map( function( k ) { return k + '=' + params.data[ k ] }).join( '&' ) : params.data || '',
            // locals
            xhr = request();

        // args
        if ( !url )
            return new Error( "Bad URL" );
//        if ( !callback )
//            return new Error( "Bad Callback function" );
        if ( !~ [ 'POST', 'GET', 'DELETE', 'PUT' ].indexOf( method ))
            return new Error( "Bad HTTP method" );

        // checking
        if ( !xhr ) throw new Error( "Browser didn't support AJAX" );

        // Setup
        // -----

        // timeout
        if ( asyncMode ) {
            if ( timeout ) xhr.timeout = timeout;
            if ( ontimeout ) xhr.ontimeout = ontimeout;
        }

        // local urls
        if ( 'file:' == window.location.protocol )
            cache = true;

        // cache responses
        if ( !cache ) {
            // generate random id and append to the url
            url += !!~ url.indexOf( '?' ) ? '?' : '&';
            url += 'random=' + Math.random().toString( 16 ).substr( 2 );
        }

        // request
        xhr.open( method, url, asyncMode );
        // post request need special headers
        if ( 'post' == params.method )
            xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );

        // Action
        // ------

        // results
        function results() {
            if ( this.readyState != 4 ) return;
            var res = {
                code: this.status,
                status: this.statusText,
                response: this.responseText || this.responseXML || ''
            };
            // results
            if ( callback )
                callback( res.code == 200 || res.code === 0 ? null : new Error( 'Ajax Error' ), res );
        }

        // async results handler
        if ( asyncMode ) xhr.onreadystatechange = results;

        // make query
        xhr.send( data );

        // sync results
        if ( !asyncMode ) results.apply( xhr );
        return xhr;
    }

    /**
     * Load file
     * ---------
     * Simple interface to load scripts via ajax (as text)
     *
     * @param {string} url
     * @param {function} callback
     * @param {object} [params]
     */
    function load( url, callback, params ) {
        params = params || {};
        params.url = url;
        params.method = 'get';
        params.callback = callback;
        ajax( params );
    }


    // Flow control
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Async functions flow
     * --------------------
     * Params:
     *
     *   {boolean} force - will not stop on errors
     *
     * @param {function[]} fns
     * @param {function} [progress]
     * @param {function} [callback]
     * @param {object} [params]
     * @returns {*}
     */
    function async( fns, progress, callback, params ) {

        // optional args
        if ( arguments.length < 3 )
            callback = progress,
            progress = undefined;
        var
            // params
            fns = ( fns || [] ).filter( function( v ) { return 'function' == typeof v }),
            progress = 'function' == typeof progress ? progress : false,
            callback = 'function' == typeof callback ? callback : false,
            params = params || {},
            force = params.force || false,

            // locals
            results = [],
            error = false,
            // counters
            i = 0,
            len = fns.length,
            counter = len;

        // return if empty functions list
        if ( !len ) return callback
            && callback();

        // start all at the same time
        for ( i; i < len; i++ ) {
            !function( i ) {
                // call function
                var fn = fns[ i ];
                setTimeout( function() {
                    if ( error && !force ) return;
                    fn( next.bind( null, i ));
                }, 0 );
            }( i );
        }

        // results
        function next( i, err, res ) {
            results[ i ] = res;
            // was error
            if ( err ) error = err;
            if ( err && !force )
                return callback( error, results );
            // progress
            if ( progress ) progress(( len - counter + 1 ) / len );
            // callback on end
            if ( !--counter )
                return callback
                    && callback( error, results );
        }
    }

    /**
     * Synchronous functions flow
     * --------------------------
     * Params:
     *
     *   {boolean} force - will not stop on errors
     *
     * @param {function[]} fns
     * @param {function} [progress]
     * @param {function} [callback]
     * @param {object} [params]
     */
    function sync( fns, progress, callback, params ) {

        // optional args
        if ( arguments.length < 3 )
            callback = progress,
            progress = undefined;
        var
            results = [],
            // params
            fns = ( fns || [] ).filter( function( v ) { return 'function' == typeof v }),
            progress = 'function' == typeof progress ? progress : false,
            callback = 'function' == typeof callback ? callback : false,
            params = params || {},
            force = params.force || false,
            len = fns.length;

        // iterate
        function next( err, res ) {
            var fn = fns.shift();

            // last call results
            if ( arguments.length )
                results.push( res );
            if ( err && !force )
                return callback
                    && callback( err, results );

            // progress
            if ( progress ) progress(( len - fns.length ) / len );

            // call next function
            if ( !fn )
                return callback
                    && callback( null, results );
            setTimeout( function() {
                fn( next );
            }, 0 );
        }
        // run
        next();
    }


    // Handle Errors
    // --------------------------------------------------------------------------------------------------------------
    window.onerror = function( msg, url, line, pos, err ) {
        console.warn( 'ERROR', msg, url, line, pos, err );
        ajax({
            url: '/error',
            method: 'post',
            data: { message: msg, url: url, line: line, pos: pos, err: err }
            // [].slice.call( arguments ).join( ', ' )/
        });
        return true;
    };

    // Tracing

    var _console = {
        log: console.log
    };
    console.log = function() {
        _console.log && _console.log.apply( console, arguments );
        var trace = '',
            args = [].slice.call( arguments );
        args.forEach( function( arg ) {
            if ( 'object' != typeof arg )
                trace += '' + arg + '\n';
            else for ( var key in arg )
                trace += key + ': ' + arg[ key ] + '\n';
        });
        ajax({
            url: '/message',
            method: 'post',
            data: trace
        });
    };



    // Init
    // ----

    // debug
    window.api = {
        // amd
        define: define,
        require: require,
        // ajax
        ajax: ajax,
        load: load,
        // flow
        async: async,
        sync: sync
    };

    init();

})();