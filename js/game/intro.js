/**
 *
 * 1. Intro
 * --------
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define(
    [
        'lib/config',
        'lib/states',
        'lib/scene',
        'lib/assets',
        'lib/browser',
        // com
        'game/com/background_gray',
        'game/com/foreground_shadow',
        'game/view/view_intro',
        'game/view/view_select'
    ],
    function( Config, States, Scene, Assets, Browser,
              // components
              BackgroundGray, Foreground,
              // views
              Intro, Select
            ) {

        'use strict';

        var
            // global

            config = Config( 'game' ),
            game_states = States( 'game' ),
            scene = Scene( 'game' ),

            // locals

            states = new States.Class(),
            layer = scene.layer( 'intro', { visible: false }),

            // components
            background = BackgroundGray(),
            foreground = Foreground(),
            intro = Intro(),
            select = Select(),

            // settings
            messages = {
                select: 'Time to select fruit',
                start: 'Let’s start the game',
                win: ' Vow! Yes you can!',
                fail: 'No chips. Try again'
            };


        // States
        // ----------------------------------------------------------------------------------------------------------

        // game states
        game_states
            .state( 'intro', enter, leave )
            .state( 'fail', failEnter, failLeave )
            .state( 'win', winEnter, winLeave )
            .event( 'next', 'intro', 'spin' )
            .event( 'next', 'fail', 'spin' )
            .event( 'next', 'win', 'spin' );

        // local intro states
        states
            .state( 'wait', waitEnter, waitLeave )
            .state( 'select', selectEnter, selectLeave )
            .state( 'ready', readyEnter, readyLeave )
            .event( 'select', 'wait', 'select' )
            .event( 'select', 'ready', 'select' )
            .event( 'next', 'select', 'ready' );

        // exit_from_intro handler
        states.on( 'end', endIntro );


        // Layers
        // ----------------------------------------------------------------------------------------------------------

        // background
        layer.layer( background.layer );
        // ui views
        layer.layer( intro.layer.hide() );
        layer.layer( select.layer.hide() );
        // foreground
        layer.layer( foreground.layer );
        foreground.shade();


        // Actions
        // ----------------------------------------------------------------------------------------------------------

        // Game Introduction
        // -----------------

        function enter() {
            // fx
            layer.show();
            background.animate();

            // basic startup routing
            if ( !config.get( 'fruit' ))
                states.setState( 'wait' );
            else states.setState( 'select' );
        }
        function leave() {
            // fx
            layer.hide();
        }

        function endIntro() { }


        // Game Over
        // ---------

        function failEnter() {
            // fx
            layer.show();
            background.animate();

            // start local 'ready' state
            // with 'game over' message
            states.setState( 'ready', messages.fail );
        }
        function failLeave() {
            // fx
            layer.hide();
        }

        // Winner
        // ------

        function winEnter() {
            // fx
            layer.show();
            background.animate();

            // call local 'ready' state
            // with victory message
            states.setState( 'ready', messages.win );

        }
        function winLeave() {
            // fx
            layer.hide();
        }

        // Wait
        // ----

        function waitEnter( message ) {

            intro.layer.get( 'message' ).text( message || messages.select );
            intro.disable();
            intro.layer.show();

            // events
            intro.events.off();
            intro.events.on( 'select', function() {
                states.select();
            });
            intro.events.on( 'spin', function() {
                // nothing to do
            });
        }
        function waitLeave() {
            // fx
            intro.layer.hide();
        }

        // Select
        // ------

        function selectEnter() {
            // fx
            select.layer.show();
            // events
            select.events.off();
            select.events.on( 'fruit', function( name ) {
                config.set( 'fruit', name );
                states.next();
            })
        }
        function selectLeave() {
            // fx
            select.layer.hide();
        }

        // Ready
        // -----

        function readyEnter( message ) {
            // fx
            intro.layer.get( 'message' ).text( message || messages.start );
            intro.enable();
            intro.fruit( config.get( 'fruit' ));
            intro.layer.show();

            // events

            // remove old event listeners
            intro.events.off();
            // listen click on 'select' button
            intro.events.on( 'select', function() {
                // open fruit selection state
                states.select();
            });
            // listen click on 'spin' button
            intro.events.on( 'spin', function() {
                // open the game
                states.end();
                game_states.next();

            });
        }

        function readyLeave() {
            // fx
            intro.layer.hide();
        }

    });