/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * 0. Preloader
 * ------------
 *
 * Warning!
 *
 * This module is executed before the assets are loaded.
 * Unlike other game modules you can't use assets here (in the constructor).
 *
 */

define(
    [
        'lib/polyfills',  // (!)
        'lib/config',
        'lib/states',
        'lib/scene',
        'lib/assets',
        'lib/events',
        'lib/dom',
        'lib/browser',

    ],
    function( polyfills, Config, States, Scene, Assets, Events, Dom, Browser ) {

        'use strict';

        var
            configUrl = 'config/game.json',
            config = Config( 'game' ),
            assets = Assets( 'game' ),
            states = States( 'game' ),
            scene = Scene( 'game', '#game', 960, 536, { clear: false, background: '#556270' }),

            // locals
            layer,
            progress,
            finish = true;


        // Bootstrap
        // ----------------------------------------------------------------------------------------------------------

        // initialize game score
        config.set( 'max_scores', 10 );
        config.set( 'score', config.get( 'max_scores' ));


        // Keep actual scene size
        // ----------------------

        if ( Browser.mobile ) {
            // resize canvas
            onResize();
            // listen resize event
            Events.on( window, 'resize', onResize );
        }
        // action
        function onResize() {
            // window.innerHeight
            var zoom_w = document.body.clientWidth / scene.canvas.canvas.width,
                zoom_h = window.innerHeight / scene.canvas.canvas.height;
            //scene.canvas.canvas.width = document.body.clientWidth;
            //scene.canvas.canvas.height = window.innerHeight;
            scene.canvas.setWidth( document.body.clientWidth );
            scene.canvas.setHeight( window.innerHeight );
            // zoom it
            scene.canvas.zoom( zoom_w, zoom_h );
        }


        // Loading
        // -------

        sync([
            function( next ) {

                // 1. Load configuration
                assets.load(
                    {
                        type: 'json',
                        url: configUrl
                    },
                    function( err, res ) {
                        // configuration
                        config.set( 'game', res );
                        next( err );
                    });
            },
            function( next ) {

                // 2. Load assets
                assets.loadGroup(
                    // get config data
                    config.get( 'game.assets' ),
                    // show progress of loading
                    onProgress,
                    // results
                    next
                );
            },
            function( next ) {

                // 3. Load game modules
                require(
                    config.get( 'game.modules' ),
                    function() {
                        var mods = [].slice.call( arguments );
                        next();
                    });
            }
        ],
        function( err, res ) {
            if ( err ) throw err;
            onComplete();
        });


        // Layers
        // ----------------------------------------------------------------------------------------------------------

        layer = scene
            .layer( 'preload' )
            .add( 'background', 'cover', { color: '#556270' })
            .add( 'progressbar', 'buffer', {
                width: scene.canvas.width,
                height: scene.canvas.height
            });

        // F R U I T S
        var ox = 0;
        'FRUITS'.split( '' )
            .forEach( function( char ) {
                layer.add( char, 'text', char, {
                    x: 240 + ox,
                    y: 210,
                    size: '121px',
                    line_height: 121,
                    color: '#f1EBEB',
                    font: 'Helvetica, Sans-serif'
                });
                ox += layer.get( char ).buffer.width;
            });


        // States
        // ----------------------------------------------------------------------------------------------------------

        states
            .state( 'preload', enter, leave )
            .event( 'next', 'preload', 'intro' )
            .setState( 'preload' );

        function enter() {
            layer.show();
        }
        function leave() {
            layer.hide();
        }


        // Actions
        // ----------------------------------------------------------------------------------------------------------

        // Events
        // ------

        // update percentage
        function onProgress( percent ) {
            // console.log( 'progress:', percent * 100 >> 0 );
            var buffer = layer.get( 'progressbar' ).buffer, oy;
            oy = scene.canvas.height - 18;
            buffer.line( 0, oy, scene.canvas.width * percent, oy, 12, '#4ECDC4', 'square' );
            oy = scene.canvas.height - 6;
            buffer.line( 0, oy, scene.canvas.width * percent, oy, 12, '#556270', 'square' );
            scene.refresh();
            // total
            progress = percent;
        }

        // TODO: loading animation
        // TODO: show the game rules

        // loading completed
        function onComplete() {
            console.log( 'Client has loaded the game!' );
            states.next();

        }

    });