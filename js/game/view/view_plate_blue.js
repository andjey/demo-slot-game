/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_plate_blue',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        // com
        'game/view/view_spin_fruits'
    ],
    function( Layer, Assets, Emitter, Spin ) {

        'use strict';

        return function() {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'view_plate_blue', { visible: true }),
                events = new Emitter(),

                // components
                spin = Spin();

            // Layers
            // -------------------------------------------------------------------------------------------------------

            // plate
            layer.add( 'plate', 'sprite', assets.get( 'plate_blue' ), { });
            layer.add( 'grid', 'sprite', assets.get( 'plate_blue_grid' ), { x: 75, y: 181, visible: false });
            // wheel
            layer.layer( spin.layer );

            // positions
            spin.layer.position( 101, -180 );

            // Animations
            // -------------------------------------------------------------------------------------------------------

            // Actions
            // -------------------------------------------------------------------------------------------------------

            function grid( show ) {
                var grid = layer.get( 'grid' );
                show ? grid.show() : grid.hide();
            }

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // actions
                grid: grid,
                // through
                fruits: spin
            };
        }
    });