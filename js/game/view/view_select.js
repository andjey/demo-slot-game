/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_select',
    [
        'lib/scene/layer',
        'lib/config',
        'lib/assets',
        'lib/emitter',
        'lib/browser',
        // com
        'game/com/button_fruit'
    ],
    function( Layer, Config, Assets, Emitter, Browser, comFruit ) {

        'use strict';

        return function() {
            var
                config = Config( 'game' ),
                assets = Assets( 'game' ),
                layer = new Layer( 'view_select' ),
                events = new Emitter(),

                // components
                buttons = {},
                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            // create buttons
            [
                { name: 'grapes', x: 80, y: 88, delay: 0 },
                { name: 'pineapple', x: 370, y: 88, delay: Math.PI / 3  },
                { name: 'strawberries', x: 657, y: 88, delay: Math.PI * 2 / 3 },
                { name: 'lemon', x: 80, y: 302, delay: Math.PI },
                { name: 'gold', x: 370, y: 302, delay: Math.PI + Math.PI / 3 },
                { name: 'watermelon', x: 657, y: 302, delay: Math.PI + Math.PI * 2 / 3 }
            ].forEach(
                function( button ) {
                    // create fruit
                    var fruit = comFruit( button.name );
                    fruit.enable();
                    fruit.layer.name = button.name;
                    layer.layer( fruit.layer );
                    fruit.layer.position( button.x, button.y );
                    // animation
                    if ( !Browser.mobile )
                    fruit.layer.animations({
                        //x: { value: - lines.source.width * 2, duration: slow },
                        y: { duration: 800,
                            value: function( progress, y ) {
                                return y + 3 * Math.cos(( Math.PI * 2 * progress ) + button.delay ) /* 0..2PI */ ;
                            }
                        }}, false, 0 );
                    // keep object
                    buttons[ button.name ] = fruit;
                    // events
                    fruit.events.on( 'click', onSelected );
                });


            // Animations
            // -------------------------------------------------------------------------------------------------------

            // Actions
            // -------------------------------------------------------------------------------------------------------

            // TODO: hide the star shadow


            // Events
            // -------------------------------------------------------------------------------------------------------

            function onSelected( name ) {
                current_fruit = name;
                events.emit( 'fruit', name );
                return false;
            }


            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events
                // actions
            };
        }
    });