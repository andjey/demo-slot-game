/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_intro',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        // com
        'game/com/button_fruit',
        'game/com/button_spin'
    ],
    function( Layer, Assets, Emitter, comFruit, comSpin ) {

        'use strict';

        return function() {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'view_intro', { visible: false }),
                events = new Emitter(),

                // components
                select = comFruit(),
                spin = comSpin(),

                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            // selector
            layer.layer( select.layer );
            // spin button
            layer.layer( spin.layer );

            // hint
            layer.add( 'message', 'text', '', {
                x: 295, y: 73,
                nowrap: true,
                size: '36px',
                line_height: 36,
                color: '#2A3138',
                font: 'Verdana, Sans-serif'
            });
            // triangle
            layer.add( 'triangle', 'sprite', assets.get( 'triangle' ), { x: 493, y: 290 });

            // positions
            select.layer.position( 250, 240 );
            spin.layer.position( 550, 240 );

            // Animations
            // -------------------------------------------------------------------------------------------------------

            // Actions
            // -------------------------------------------------------------------------------------------------------

            // TODO: hide star shadow

            function
                fruit( name ) {
                if ( !~select.names.indexOf( name )) return;
                current_fruit = name;
                select.fruit( name );
            }

            function enable() {
                select.enable();
                spin.enable();
                spin.ready();   // animation
            }
            function disable() {
                select.disable();
                select.waiting();   // animation
                spin.disable();
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            select.events.on( 'click', function() {
                events.emit( 'select' );
            });

            spin.events.on( 'click', function() {
                events.emit( 'spin' );
            });

            select.events.on( 'fruit', function( name ) {
            });

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // parts
                message: layer.get( 'message' ),
                // actions
                fruit: fruit,
                enable: enable,
                disable: disable
            };
        }
    });