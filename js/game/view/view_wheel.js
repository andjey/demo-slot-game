/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_wheel',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        'lib/browser',
        // com
        'game/com/button_fruit',
        'game/com/button_spin'
    ],
    function( Layer, Assets, Emitter, Browser, comFruit, comSpin ) {

        'use strict';

        return function( plates ) {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'view_wheel', { visible: false }),
                events = new Emitter(),

                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            layer.layer( plates.red.layer );
            layer.layer( plates.blue.layer );
            layer.layer( plates.green.layer );

            // zero positions (right out of screen)
            plates.red.layer.position( 960, 0 );
            plates.blue.layer.position( 960, 0 );
            plates.green.layer.position( 960, 0 );
            if ( Browser.mobile )
                plates.green.controls.layer.x = 960;


            // Animations
            // -------------------------------------------------------------------------------------------------------

            function show( callback ) {
                // animation duration
                var speed = 750;

                // stop older animations
                layer.stopAll();

                // show current layer
                layer.show();

                if ( Browser.mobile ) {
                    plates.red.layer.position( -95, 0 );
                    plates.blue.layer.position( 230, 0 );
                    plates.green.layer.position( 565, 0 );
                    plates.green.controls.layer.x = 960;
                    callback && callback();
                }
                else
                // animate color plates
                async([

                    // red
                    function( next ) {
                        plates.red.layer.animate( 'x', -95, speed * .9, { delay: speed * .4 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.red.layer.get( 'view_spin_fruits' ).animate( 'x', 121, speed, next.bind( this, false ));
                    },

                    // blue
                    function( next ) {
                        plates.blue.layer.animate( 'x', 230, speed * .7, { delay: speed * .3 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.blue.layer.get( 'view_spin_fruits' ).animate( 'x', 121, speed, next.bind( this, false ));
                    },

                    // green
                    function( next ) {
                        plates.green.layer.animate( 'x', 565, speed, { delay: 0 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.green.layer.get( 'view_spin_fruits' ).animate( 'x', 121, speed, next.bind( this, false ));
                    },

                    // controls
                    function( next ) {
                        plates.green.controls.layer.animate( 'x', 500, 1000, next.bind( this, false ));
                    }

                ], callback );
            }

            function hide( callback ) {
                // animation duration
                var speed = 500;

                // stop older animations
                layer.stopAll();

                if ( Browser.mobile ) {
                    plates.red.layer.position( 960, 0 );
                    plates.blue.layer.position( 960, 0 );
                    plates.green.layer.position( 960, 0 );
                    plates.green.controls.layer.x = 960;
                    callback && callback();
                }
                else
                async([
                    function( next ) {
                        plates.red.layer.animate( 'x', 960, speed, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.blue.layer.animate( 'x', 960, speed * .8, { delay: speed * .5 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.green.layer.animate( 'x', 960, speed * .7, { delay: speed }, next.bind( this, false ));
                    }
                ], function() {
                    layer.hide();
                    callback && callback();
                });
            }

            function mark( show ) {
                plates.green.grid( show );
                plates.blue.grid( show );
                plates.red.grid( show );
            }

            // Actions
            // -------------------------------------------------------------------------------------------------------

            /**
             * Prepare and show fruits from the queue
             */
            function showFruits() {
                plates.green.fruits.reset();
                plates.green.fruits.show(
                    Browser.mobile ? 12 : 35, 0 );
                plates.blue.fruits.reset();
                plates.blue.fruits.show(
                    Browser.mobile ? 9 : 25, 0 );
                plates.red.fruits.reset();
                plates.red.fruits.show(
                    Browser.mobile ? 9 : 15, 0 );
            }

            /**
             * Spin the wheel
             * @param {function} [callback]
             */
            function spin( callback ) {
                // spin
                plates.green.fruits.scroll( function() {
                    callback && callback();
                });
                plates.blue.fruits.scroll();
                plates.red.fruits.scroll();
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            // Init
            // -------------------------------------------------------------------------------------------------------


            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // actions
                show: show,
                hide: hide,
                mark: mark,
                showFruits: showFruits,
                spin: spin
            };
        }
    });