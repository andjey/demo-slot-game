/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_controls',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/config',
        'lib/emitter',
        // com
    ],
    function( Layer, Assets, Config, Emitter ) {

        'use strict';

        return function() {
            var
                assets = Assets( 'game' ),
                config = Config( 'game' ),
                layer = new Layer( 'view_controls', { visible: true }),
                events = new Emitter(),

                // locals

                fruit_layer,
                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            // plate
            layer.add( 'plate', 'sprite', assets.get( 'game_ui' ));
            layer.add( 'over', 'sprite', assets.get( 'game_ui_over' ), { x: 9, y: 110, visible: false });
            // spin
            layer.add( 'spin', 'sprite', assets.get( 'spin' ), { x: 27, y: 132, alpha_level: 75 });
            // white fruit icons
            fruit_layer = layer.layer( new Layer( 'white_fruit' ));
            // scores
            layer.add( 'score', 'text', '' + ( config.get( 'score' ) || '10' ), {
                x: 63, y: 30,
                nowrap: true,
                size: '70px',
                line_height: 70,
                color: '#FFFFFF',
                font: 'Helvetica, Sans-serif'
            });

            // positions
            //controls.layer.position( 0, 0 );

            // Animations
            // -------------------------------------------------------------------------------------------------------

            function ready() {
                layer.get( 'spin' )
                    .animations({
                        y: {
                            value: function( progress, y ) {
                                return y + 15 * ( 1 - progress ) * ( - Math.sin( Math.PI * 2 * progress * 3 ));
                            },
                            duration: 800,
                            delay_after: 3000
                        }
                    }, false, 0 );
            }

            // Actions
            // -------------------------------------------------------------------------------------------------------

            /**
             * Show game score
             * @param [amount]
             */
            function score( amount ) {
                var amount = undefined === amount ? config.get( 'score' ) : amount;
                layer.get( 'score' ).text( '' + ( amount || 0 ));
                config.set( 'score', amount );
            }


            /**
             * Set white fruit
             * @param [name]
             */
            function fruit( name ) {
                var name = name || config.get( 'fruit' );
                var image = assets.get( 'white_' + name );
                if ( !image ) return;
                fruit_layer.removeAll();
                fruit_layer.add( name, 'sprite', image, { x: 35, y: 270 });
                current_fruit = name;
                var sprite = fruit_layer.get( name );
                sprite && setupFruitEvents( sprite );
                config.set( 'fruit', name );
            }

            // rollover

            function over() {
                layer.get( 'over' ).show();
                return false;
            }
            function out() {
                layer.get( 'over' ).hide();
                return false;
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            layer.get( 'spin' )
                .on( 'over', over )
                .on( 'out', out )
                .on( 'press', over )
                .on( 'release', out )
                .on( 'click', function() {
                    events.emit( 'spin' );
                    return false;
                });

            function setupFruitEvents( sprite ) {
                sprite.on( 'click', function() {
                    events.emit( 'fruit' );
                    return false;
                });

            }

            // Init
            // -------------------------------------------------------------------------------------------------------

            // animate it
            ready();

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // parts
                score: score,
                fruit: fruit
            };
        }
    });