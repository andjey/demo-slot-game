/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_results',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        'lib/browser',
        // com
        'game/com/button_fruit',
        'game/com/button_spin'
    ],
    function( Layer, Assets, Emitter, Browser, comFruit, comSpin ) {

        'use strict';

        return function( plates ) {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'view_results', { visible: false }),
                events = new Emitter(),

                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            layer.layer( plates.red.layer );
            layer.layer( plates.blue.layer );
            layer.layer( plates.green.layer );

            // zero positions (right out of screen)
            plates.red.layer.position( 960, 0 );
            plates.blue.layer.position( 960, 0 );
            plates.green.layer.position( 960, 0 );
            if ( Browser.mobile )
                plates.green.controls.layer.x = 960;


            // Animations
            // -------------------------------------------------------------------------------------------------------

            function show( callback ) {
                // animation duration
                var speed = 750;

                // stop older animations
                layer.stopAll();

                // show current layer
                layer.show();

                if ( Browser.mobile ) {
                    plates.red.layer.position( -95, 0 );
                    plates.blue.layer.position( 165, 0 );
                    plates.green.layer.position( 417, 0 );
                    plates.green.controls.layer.x = 336;
                    callback && callback();
                }
                else
                // animate colored plates
                async([

                    // red
                    function( next ) {
                        plates.red.layer.animate( 'x', -95, speed, { delay: speed * .3 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.red.layer.get( 'view_spin_fruits' ).animate( 'x', 95, speed, next.bind( this, false ));
                    },

                    // blue
                    function( next ) {
                        plates.blue.layer.animate( 'x', 165, speed, { delay: speed * .125 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.blue.layer.get( 'view_spin_fruits' ).animate( 'x', 80, speed, next.bind( this, false ));
                    },

                    // green
                    function( next ) {
                        plates.green.layer.animate( 'x', 417, speed, { delay: 0 }, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.green.layer.get( 'view_spin_fruits' ).animate( 'x', 80, speed, next.bind( this, false ));
                    },

                    // controls
                    function( next ) {
                        plates.green.controls.layer.animate( 'x', 336, speed, next );
                    }

                ], callback );
            }

            function hide( callback ) {
                // stop older animations
                layer.stopAll();

                if ( Browser.mobile ) {
                    plates.red.layer.position( 960, 0 );
                    plates.blue.layer.position( 960, 0 );
                    plates.green.layer.position( 960, 0 );
                    plates.green.controls.layer.x = 960;
                    callback && callback();
                }
                else
                // animate colored plates
                async([
                    function( next ) {
                        plates.red.layer.animate( 'x', 960, 1000, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.blue.layer.animate( 'x', 960, 1000, next.bind( this, false ));
                    },
                    function( next ) {
                        plates.green.layer.animate( 'x', 960, 1000, next.bind( this, false ));
                    },
                    // controls
                    function( next ) {
                        plates.green.controls.layer.animate( 'x', 500, 1000, next.bind( this, false ));
                    }
                ], function() {
                    layer.hide();
                    callback && callback();
                });
            }

            // Actions
            // -------------------------------------------------------------------------------------------------------

            // Events
            // -------------------------------------------------------------------------------------------------------

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // actions
                show: show,
                hide: hide
            };
        }
    });