/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */


define( 'view_spin_fruits',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/browser',
        // com
        'game/com/button_fruit'
    ],
    function( Layer, Assets, Browser, Fruit ) {

        'use strict';

        return function() {
            var
                layer = new Layer( 'view_spin_fruits', { visible: true }),
                assets = Assets( 'game' ),

                // locals

                fruits = {
                    gold_small: true,
                    strawberries: true,
                    pineapple: true,
                    lemon: true,
                    watermelon: true,
                    grapes: true
                },
                names = Object.keys( fruits ),
                zero_y = -180,

                // spin
                height_offset = 180,
                stack = [],
                stack_length = 5,
                last_shown = 0;

            // Layers
            // ---------------------------------------------------------------------------------------------------

            // Animations
            // ---------------------------------------------------------------------------------------------------

            /**
             * Spin the wheel
             * @param num
             * @param callback
             */
            function scroll( num, callback ) {
                // optional args
                if ( 'function' == typeof num )
                    callback = num,
                        num = undefined;
                var num = num || stack_length || 0;
                // spin
                if ( num < 2 ) num = 2;


                var duration = num * 100;
                if ( Browser.mobile ) duration = num * 300;

                layer.animate( 'y', ( num - 2 ) * height_offset, duration, { repeats: 1 },
                    // stretch stop
                    function() {
                        layer.animate( 'y', function( progress, y ) {
                            var x = Math.PI * 2 * progress;
                            return y - 50 * Math.sin( x ) * ( !x ? 1 : Math.sin( x ) / x );
                        }, 1000, callback );
                    });
            }

            function composeFruits() {

            }

            // Actions
            // ---------------------------------------------------------------------------------------------------

            /**
             * Create necessary fruit objects
             * @param [skip] - how much skip to prevent duplicates
             */
            function compose( skip ) {
                var fruit,
                    index;
                for ( var i = skip || 0; i < stack.length; i ++ ) {
                    fruit = stack[ i ];
                    index = names.indexOf( fruit.type );
                    if ( !~index ) return;  // not exists
                    // create fruit sprite
                    layer.add( fruit.id, 'sprite', assets.get( fruit.type ), { visible: false });
                }
            }

            /**
             * Generate random fruit queue
             * @param [num] - amount of fruits to generate
             */
            function randomize( num ) {
                var rnd,
                    skip = stack.length;
                if ( num ) stack_length = + num >> 0;  // int
                // generate stack
                for ( var i = stack.length; i < stack_length; i ++ ) {
                    rnd = Math.random() * names.length >> 0;  // int
                    stack.push({
                        id: Math.random().toString( 16 ).substr( 2 ),  // uid
                        type: names[ rnd ]
                    });
                }
                // create new sprites
                compose( skip );
            }

            /**
             * Show part of fruit queue
             * @param [num] - number of fruits which must be in queue
             * @param [offset] - number of rows to slide down after
             */
            function show( num, offset ) {
                var
                    num = num || stack_length >> 0,
                    y = 0,
                    fruit,
                    sprite;

                if ( num != stack.length )
                    randomize( num );
                last_shown = offset || num;

                // show new fruits
                for ( var i = 0; i < num && i < stack.length; i ++ ) {
                    // get sprite from stack
                    fruit = stack[ i ];
                    sprite = fruit && layer.get( fruit.id );
                    if ( !sprite ) continue;
                    // setup position
                    sprite.y = y;
                    y -= height_offset;
                    // show it
                    sprite.show();
                }
                if ( offset )
                    layer.scroll( 0, offset * 180, 1500 );
                else
                if ( 0 !== offset )
                    layer.y = 3 * 180;
            }

            /**
             * Reset fruit queue (prepare new spin)
             */
            function reset() {
                // create new stack
                if ( !stack.length ) {
                    randomize();
                    layer.y = zero_y;
                // prepare stack for new spin
                } else {
                    var num = last_shown - 4;
                    if ( num < 0 ) num = 0;
                    // remove most of old fruits
                    var removed = stack.splice( 0, num );
                    removed.forEach( function( fruit ) {
                        layer.removeChild( fruit.id );
                    });
                    // update position of leaved fruits
                    var y = 0;
                    stack.forEach( function( fruit ) {
                        layer.get( fruit.id ).y = y;
                        y -= height_offset;
                    });
                    layer.y = zero_y + 3 * height_offset;
                    last_shown = 3;
                }
            }

            /**
             * Clear queue
             */
            function clear() {
                // remove old sprites
                layer.removeAll();
                // clear stack
                stack = [];
                last_shown = 0;
            }

            /**
             * Get currently visible fruits (3 in column)
             * (to calculate winning situation)
             * @returns {string[]}
             */
            function getFruits() {
                var first = Math.max( last_shown - 4, 0 ),
                    results = stack.slice( first, last_shown - 1 )
                        .map( function( obj ) {
                            // normalize fruit names
                            if ( 'gold_small' == obj.name ) return 'gold';
                            // return array of names
                            return obj.type
                        });
                return results.reverse();
            }

            // Events
            // ---------------------------------------------------------------------------------------------------


            // Init
            // ---------------------------------------------------------------------------------------------------

            // fill stack
            randomize();


            // API
            // ---------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                // animations
                scroll: scroll,
                // actions
                randomize: randomize,
                show: show,
                reset: reset,
                // get visible fruits list (3 in column)
                getFruits: getFruits

            };
        }
    });