/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'view_plate_green',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        // com
        'game/view/view_spin_fruits',
        'game/view/view_controls'
    ],
    function( Layer, Assets, Emitter, Spin, Controls ) {

        'use strict';

        return function() {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'view_plate_green', { visible: true }),
                events = new Emitter(),

                // components
                spin = Spin(),
                controls = Controls(),

                current_fruit;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            // plate
            layer.add( 'plate', 'sprite', assets.get( 'plate_green' ), { });
            layer.add( 'grid', 'sprite', assets.get( 'plate_green_grid' ), { x: 71, y: 164, visible: false });
            layer.layer( spin.layer );
            // controls
            layer.layer( controls.layer );

            // positions
            spin.layer.position( 101, -180 );
            controls.layer.position( 336, 80 );

            // Animations
            // -------------------------------------------------------------------------------------------------------

            // Actions
            // -------------------------------------------------------------------------------------------------------

            /**
             * Show/hide grid on the plate
             * @param [show]
             */
            function grid( show ) {
                var grid = layer.get( 'grid' );
                show ? grid.show() : grid.hide();
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // actions
                grid: grid,
                // through
                fruits: spin,
                controls: controls
            };
        }
    });