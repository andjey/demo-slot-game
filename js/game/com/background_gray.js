/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */


define( 'background_gray',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/browser'
    ],
    function( Layer, Assets, Browser ) {

        'use strict';

        return function() {
            var
                layer = new Layer( 'background_gray', { visible: true }),
                assets = Assets( 'game' ),
                stripes,
                lines;

            // Layers
            // ---------------------------------------------------------------------------------------------------

            layer
                .add( 'stripes', 'cover', { image: assets.get( 'stripes' ), x: 0 })
                .add( 'lines', 'cover', { image: assets.get( 'lines' ) })
                .add( 'noise', 'cover', { image: assets.get( 'noise' ) })

            // Animations
            // ---------------------------------------------------------------------------------------------------

            function animate() {
                if ( Browser.mobile ) return;

                // moving stripes
                var stripes = layer.get( 'stripes' );
                stripes.slide( - stripes.source.width, 0, 10000, 0 );

                // moving lines
                var lines = layer.get( 'lines' ),
                    slow = 3000;
                lines.animations({
                    x: { value: - lines.source.width * 2, duration: slow },
                    y: { duration: slow * 4,
                        value: function( progress, y ) {
                            return y + 50 * Math.cos( Math.PI * 2 * progress ) /* 0..2PI */ ;
                      }
                  }}, false, 0 );
            }

            // Events
            // ---------------------------------------------------------------------------------------------------


            // API
            // ---------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                // animations
                animate: animate
            };
        }
    });