/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'button_spin',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        'lib/browser'
    ],
    function( Layer, Assets, Emitter, Browser ) {

        'use strict';

        return function() {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'button_spin', { visible: true }),
                events = new Emitter(),

                // locals
                enabled,
                disabled,
                active_layer,
                is_enable;

            // Layers
            // -------------------------------------------------------------------------------------------------------

            disabled = layer.layer( 'disabled', { visible: false })
                .add( 'bg', 'sprite', assets.get( 'spin_disabled_bg' ), {  })
                .add( 'over', 'sprite', assets.get( 'spin_disabled_bg_over' ), {  })
                .add( 'spin', 'sprite', assets.get( 'spin_disabled' ), { x: 17, y: 20, alpha_level: 75 });

            enabled = layer.layer( 'enabled', { visible: false })
                .add( 'bg', 'sprite', assets.get( 'spin_bg' ), {  })
                .add( 'over', 'sprite', assets.get( 'spin_bg_over' ), {  })
                .add( 'spin', 'sprite', assets.get( 'spin' ), { x: 17, y: 21, alpha_level: 32 });

            // Animations
            // -------------------------------------------------------------------------------------------------------

            function notAllowed() {
                if ( Browser.mobile ) return;

                active_layer.get( 'spin' )
                    .animations({
                        x: {
                            value: function( progress, y ) {
                                return y + 10 * ( Math.sin( Math.PI * 2 * progress * 2.5)) /* 0..2PI */ ;
                            },
                            duration: 400
                        }
                    });
            }

            function ready() {
                if ( Browser.mobile ) return;

                active_layer.get( 'spin' )
                    .animations({
                        y: {
                            value: function( progress, y ) {
                                return y + 15 * ( 1 - progress ) * ( - Math.sin( Math.PI * 2 * progress * 3 )) /* 0..2PI */ ;
                            },
                            duration: 800,
                            delay_after: 3000
                        }
                    }, false, 0 );
            }

            // Actions
            // -------------------------------------------------------------------------------------------------------

            // activation

            function enable() {
                disabled.hide();
                active_layer = enabled.show();
                is_enable = true;
                return out();
            }
            function disable() {
                enabled.hide();
                active_layer = disabled.show();
                is_enable = false;
                return out();
            }

            // rollover

            function over() {
                active_layer.get( 'bg' ).hide();
                active_layer.get( 'over' ).show();
                return false;
            }
            function out() {
                active_layer.get( 'bg' ).show();
                active_layer.get( 'over' ).hide();
                return false;
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            disabled.get( 'spin' )
                .on( 'over', over )
                .on( 'out', out )
                .on( 'press', over )
                .on( 'release', out )
                .on( 'click', function() {
                    notAllowed();
                    events.emit( 'click' );
                    return false;
                });

            enabled.get( 'spin' )
                .on( 'over', over )
                .on( 'out', out )
                .on( 'press', over )
                .on( 'release', out )
                .on( 'click', function() {
                    notAllowed();
                    events.emit( 'click' );
                    return false;
                });


            // API
            // -------------------------------------------------------------------------------------------------------

            // init
            disable();

            return {
                layer: layer,
                events: events,
                // actions
                enable: enable,
                disable: disable,
                // animations
                notAllowed: notAllowed,
                ready: ready
            };
        }
    });