/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */


define( 'foreground_shadow',
    [
        'lib/scene/layer',
        'lib/assets'
    ],
    function( Layer, Assets ) {

        'use strict';

        return function() {
            var
                layer = new Layer( 'foreground_shadow', { visible: true }),
                assets = Assets( 'game' ),
                stripes,
                lines;

            // Layers
            // ---------------------------------------------------------------------------------------------------

            layer
                .add( 'shade', 'sprite', assets.get( 'shade' ))
                .add( 'shade2', 'sprite', assets.get( 'shade' ), { visible: false });

            // Animations
            // ---------------------------------------------------------------------------------------------------

            function animate() {
                // TODO
            }

            // Actions
            // ---------------------------------------------------------------------------------------------------

            function light() {
                layer.get( 'shade2' ).hide();
            }
            function shade() {
                layer.get( 'shade2' ).show();
            }

            // Events
            // ---------------------------------------------------------------------------------------------------


            // API
            // ---------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                // animations
                animate: animate,
                // actions
                light: light,
                shade: shade
            };
        }
    });