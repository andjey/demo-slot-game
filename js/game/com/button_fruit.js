/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'button_fruit',
    [
        'lib/scene/layer',
        'lib/assets',
        'lib/emitter',
        'lib/browser'
    ],
    function( Layer, Assets, Emitter, Browser ) {

        'use strict';

        return function( name ) {
            var
                assets = Assets( 'game' ),
                layer = new Layer( 'button_fruit', { visible: true }),
                events = new Emitter(),

                active_layer,
                active_fruit_name = 'strawberries',

                names = [ 'gold', 'strawberries', 'pineapple', 'lemon', 'watermelon', 'grapes' ],
                disabled,
                enabled,
                enabled_container;

            // Layers
            // -------------------------------------------------------------------------------------------------------


            disabled = layer
                .layer( 'disabled', { visible: false })
                .add( 'bg', 'sprite', assets.get( 'fruit_disabled_bg' ), {  x: -7, y: -17 })
                .add( 'over', 'sprite', assets.get( 'fruit_disabled_bg_over' ), {  x: -7, y: -17 })
            disabled.layer( 'fruit' )
                .add( 'fruit', 'sprite', assets.get( 'fruit_disabled' ), { x: 14, y: -15, alpha_level: 75 }); // { x: 14, y: -15, alpha_level: 75 });
            // events
            setupEvents( null, disabled.get( 'fruit.fruit' ));

            enabled = layer
                .layer( 'enabled', { visible: false, x: 300 })
                .add( 'bg', 'sprite', assets.get( 'fruit_bg' ), {  x: -7, y: -17 })
                .add( 'over', 'sprite', assets.get( 'fruit_bg_over' ), {  x: -7, y: -17 })
            enabled_container = enabled.layer( 'fruit' );


            // create fruit layers
            names.forEach( function( name ) {
                enabled_container.layer( name, { visible: false })
                    .add( 'shadow', 'sprite', assets.get( name + '_shadow' ), { x: -7, y: -17 })
                    .add( 'fruit', 'sprite', assets.get( name ), { x: -7, y: -17, alpha_level: 75 });
                // events
                setupEvents( name, enabled_container.get( name + '.fruit' ));
            });

            // Animations
            // -------------------------------------------------------------------------------------------------------

            function waiting() {
                if ( Browser.mobile ) return;
                active_layer.get( 'fruit' )
                    .animations({
                        y: {
                            value: function( progress, y ) {
                                return y + 15 * ( 1 - progress ) * ( - Math.sin( Math.PI * 2 * progress * 3 )) /* 0..2PI */ ;
                            },
                            duration: 800,
                            delay_after: 3000
                        }
                    }, false, 0 );
            }

            function clicked( callback ) {
                if ( Browser.mobile ) return;
                active_layer.get( 'fruit' )
                    .stop( true )
                    .animations({
                        // TODO
                    });
            }

            // Actions
            // -------------------------------------------------------------------------------------------------------

            // TODO: hide star shadow

            function enable() {
                layer.get( 'disabled' ).hide();
                active_layer = layer.get( 'enabled' ).show();
                return out();
            }
            function disable() {
                layer.get( 'enabled' ).hide();
                active_layer = layer.get( 'disabled').show();
                return out();
            }

            function over() {
                active_layer.get( 'bg' ).hide();
                return active_layer.get( 'over' ).show();

            }
            function out() {
                active_layer.get( 'bg' ).show();
                return active_layer.get( 'over' ).hide();
            }

            function fruit( name ) {
                var layer;
                // hide old
                if ( active_fruit_name ) {
                    layer = enabled.get( 'fruit.' + active_fruit_name );
                    layer && layer.hide();
                }
                // set active
                if ( !~names.indexOf( name )) return;
                active_fruit_name = name;
                // show active
                layer = enabled.get( 'fruit.' + name );
                layer && layer.show();
                return name;
            }

            // Events
            // -------------------------------------------------------------------------------------------------------

            function setupEvents( name, sprite ) {

                sprite
                    .on( 'over', onOver )
                    .on( 'out', onOut )
                    .on( 'click', onClick );

//                layer.get( 'enabled.fruit.fruit' )
//                    .on( 'over', onOver )
//                    .on( 'out', onOut )
//                    .on( 'click', onFruit );

                function onOver( x, y ) {
                    over();
                    return false;
                }
                function onOut( x, y ) {
                    out();
                    return false;
                }
                function onClick( x, y ) {
                    clicked();
                    events.emit( 'click', name );
                    return false;
                }
//                function onFruit( name ) {
//                    events.emit( 'fruit', name );
//                }
            }

            // init
            fruit( name );

            // API
            // -------------------------------------------------------------------------------------------------------

            return {
                layer: layer,
                events: events,
                // properties
                names: names,
                // actions
                enable: enable,
                disable: disable,
                fruit: fruit,
                // animations
                waiting: waiting
            };
        }
    });