/**
 *
 * 1. Spin
 * -------
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define(
    [
        'lib/config',
        'lib/states',
        'lib/scene',
        'lib/assets',
        // com
        'game/com/background_gray',
        'game/com/foreground_shadow',
        // common views
        'game/view/view_plate_green',
        'game/view/view_plate_blue',
        'game/view/view_plate_red',
        // views
        'game/view/view_results',
        'game/view/view_wanted',
        'game/view/view_wheel',
    ],
    function( Config, States, Scene, Assets,
              comBackground, comForeground,  // com
              uiGreenPlate, uiBLuePlate, uiRedPlate,  // common views
              uiResults, uiWanted, uiWheel  // views
            ) {

        'use strict';

        var
            // global

            config = Config( 'game' ),
            assets = Assets( 'game' ),
            game_states = States( 'game' ),
            scene = Scene( 'game' ),

            // locals

            states = new States.Class(),
            layer = scene.layer( 'intro', { visible: false }),

            // components
            background = comBackground(),
            foreground = comForeground(),
            // common views
            green_plate = uiGreenPlate(),
            blue_plate = uiBLuePlate(),
            red_plate = uiRedPlate(),
            // views
            wheel = uiWheel({ green: green_plate, blue: blue_plate, red: red_plate }),
            results = uiResults({ green: green_plate, blue: blue_plate, red: red_plate }),
            wanted = uiWanted({ green: green_plate, blue: blue_plate, red: red_plate });



        // States
        // ----------------------------------------------------------------------------------------------------------

        game_states
            .state( 'spin', enter, leave )
            .event( 'select', 'spin', 'intro' )
            .event( 'win', 'spin', 'win' )
            .event( 'fail', 'spin', 'fail' );
        states
            .state( 'wheel', wheelEnter, wheelLeave )
            .state( 'results', resultsEnter, resultsLeave )
            .event( 'next', 'wheel', 'results' )
            .event( 'next', 'results', 'wheel' );
        states.on( 'end', endSpin );

        // Layers
        // ----------------------------------------------------------------------------------------------------------

        // background
        layer.layer( background.layer );
        // ui
        layer.layer( wheel.layer.hide() );
        layer.layer( results.layer.hide() );
        layer.layer( wanted.layer.hide() );
        // foreground
        layer.layer( foreground.layer );


        // Actions
        // ----------------------------------------------------------------------------------------------------------

        // Check for win
        function checkWin( target_fruit ) {

            var lines = [];
            lines.push( red_plate.fruits.getFruits() );
            lines.push( blue_plate.fruits.getFruits() );
            lines.push( green_plate.fruits.getFruits() );

            // detect win
            var win_line;
            for ( var n = 0; n < 3; n ++ ) {
                win_line = true;
                for ( var m = 0; m < 3; m ++ )
                    if ( lines[ m ][ n ] != target_fruit ) {
                        // fail this line
                        win_line = false;
                        break;
                    }
                // win if any line filled by target fruit
                if ( win_line ) {
                    console.log( 'Win (' + target_fruit + '):', lines[ 0 ], lines[ 1 ], lines[ 2 ] );
                    return true;
                }
            }
            console.log( 'Fail (' + target_fruit + '):', lines[ 0 ], lines[ 1 ], lines[ 2 ] );
            return false;
        }


        // Game Introduction
        // -----------------

        function enter() {
            // fx
            layer.show();
            states.setState( 'wheel' );
        }
        function leave() {
            // fx
            states.end();
            layer.hide();
        }

        function endSpin() {
            // fx
            layer.hide();
        }


        // Wheel
        // -----

        function wheelEnter() {

            // check and reduce scores
            var scores = config.get( 'score' );

            // game already over
            if ( scores < 1 ) {
                game_states.fail();
                return;
            }
            // reduce scores
            green_plate.controls.score( -- scores );
            // then spin the wheel again

            sync([

                function( next ) {
                    // show the grid of central row
                    wheel.mark( true );
                    // show columns with fruits
                    wheel.showFruits();
                    // next step
                    setTimeout( next, 300 );
                },
                function( next ) {
                    // fx, change state of screen
                    wheel.show( function() {
                        setTimeout( next, 1000 );
                    });
                },
                function( next ) {
                    // make screen mode shade
                    foreground.shade();
                    // and spin the wheel
                    wheel.spin( next );
                    // then change state on finish
                },
                function() {
                    // is won
                    if ( checkWin( config.get( 'fruit' )) ) {
                        // reset scores
                        config.set( 'score', config.get( 'max_scores' ));
                        wheel.hide( game_states.win );
                    } else
                    // is fail
                    if ( scores < 1 ) {
                        config.set( 'score', config.get( 'max_scores' ));
                        wheel.hide( game_states.fail );
                    } else
                    // continue playing
                        states.next();
                }
            ]);

        }

        function wheelLeave() { }

        // Results
        // -------

        function resultsEnter() {
            // fx
            green_plate.controls.fruit();
            results.show( function() {

                // light shadow
                foreground.light();

            });
            green_plate.grid( true );

            // events

            // setTimeout( states.next, 2000 );

            green_plate.controls.events.off();
            green_plate.controls.events.on( 'spin', function() {

                // stop events
                green_plate.controls.events.off();
                // lets spin the wheel
                // (open wheel state)
                states.next();
            });
            green_plate.controls.events.on( 'fruit', function() {
                config.set( 'fruit' );
                results.hide( game_states.select );
            })
        }

        function resultsLeave() {
            // fx
            green_plate.controls.events.off();
        }

    });