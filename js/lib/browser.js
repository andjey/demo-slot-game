/**
 *
 * Browser detection
 * -----------------
 *
 * Determines:
 *
 *  * kind of browser
 *  * version
 *  * os version
 *
 * regexps taken from [here](https://github.com/ded/bowser)
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'browser',
    function() {
        'use strict';

        var browser = detect( navigator ? navigator.userAgent : '' );


        /**
         * Detect browser
         * @param {string} userAgent
         */
        function detect( userAgent ) {
            var ua = userAgent;

            function first( regexp ) {
                var match = ua.match( regexp );
                return ( match && match.length > 1 && match[ 1 ]) || '';
            }

            var
                result = {},
                version = first( /version\/(\d+(\.\d+)?)/i ),

                // flags

                platforms = {
                    ios: /(ipod|iphone|ipad)/i.test( ua ),
                    likeAndroid: /like android/i.test( ua ),
                    android: /android/i.test( ua ) && !/like android/i.test( ua ),
                    tablet: /tablet/i.test( ua )
                },

                // checks

                browsers = [
                    {
                        // level 1

                        opera: {
                            re: /opera|opr/i,
                                ver: version || /(?:opera|opr)[\s\/](\d+(\.\d+)?)/i
                        },
                        windowsphone: {
                            re: /windows phone/i,
                            ver: /iemobile\/(\d+(\.\d+)?)/i,
                            os: /windows phone (?:os)?\s?(\d+(\.\d+)*)/i
                        },
                        ie: {
                            re: /msie|trident/i,
                            ver: /(?:msie |rv:)(\d+(\.\d+)?)/i
                        },
                        chrome: {
                            re: /chrome|crios|crmo/i,
                            ver: /(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i
                        },

                        iphone: {
                            re: 'iphone' == platforms.ios,
                            ver: version
                        },
                        ipad: {
                            re: 'ipad' == platforms.ios,
                            ver: version
                        },
                        ipod: {
                            re: 'ipod' == platforms.ios,
                            ver: version
                        },

                        sailfish: {
                            re: /sailfish/i,
                            ver: /sailfish\s?browser\/(\d+(\.\d+)?)/i
                        },
                        seamonkey: {
                            re: /seamonkey\//i,
                            ver: /seamonkey\/(\d+(\.\d+)?)/i
                        },
                        firefox: {
                            re: /firefox|iceweasel/i,
                            ver: /(?:firefox|iceweasel)[ \/](\d+(\.\d+)?)/i
                        },
                        silk: {
                            re: /silk/i,
                            ver: /silk\/(\d+(\.\d+)?)/i
                        },
                        phantom: {
                            re: /phantom/i,
                            ver: /phantomjs\/(\d+(\.\d+)?)/i
                        },
                        blackberry: {
                            re: /blackberry|\bbb\d+/i.test( ua ) ||
                                /rim\stablet/i.test( ua ),
                            ver: version || /blackberry[\d]+\/(\d+(\.\d+)?)/i,
                            os: /rim\stablet\sos\s(\d+(\.\d+)*)/i
                        },
                        webos: {
                            re: /(web|hpw)os/i,
                            ver: version || /w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i,
                            os: /(?:web|hpw)os\/(\d+(\.\d+)*)/i
                        },
                        bada: {
                            re: /bada/i,
                            ver: /dolfin\/(\d+(\.\d+)?)/i,
                            os: /bada\/(\d+(\.\d+)*)/i
                        },
                        tizen: {
                            re: /tizen/i,
                            ver: /(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i,
                            os: /tizen[\/\s](\d+(\.\d+)*)/i
                        },
                        safari: {
                            re: /safari/i,
                            ver: version
                        }
                    },
                    {
                        // level 2: complex

                        ios: {
                            re: platforms.ios,
                            ver: version,
                            os: function() {
                                var ver = first( /os (\d+([_\s]\d+)*) like mac os x/i );
                                return ver.replace( /[_\s]/g, '.' );
                            }
                        },
                        firefoxos: {
                            re: /firefox|iceweasel/i.test( ua ) &&
                                /\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test( ua ),
                            ver: /(?:firefox|iceweasel)[ \/](\d+(\.\d+)?)/i
                        },
                        touchpad: {
                            re: /(web|hpw)os/i.test( ua) &&
                                /touchpad\//i.test( ua ),
                            ver: version || /w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i
                        }
                    },
                    {
                        // level 3: post flags

                        webkit: {
                            re: /(apple)?webkit/i,
                            ver: function() {
                                return result.version || version;
                            }
                        },
                        gecko: {
                            re: !result.opera && /gecko\//i.test( ua ),
                            ver: version || /gecko\/(\d+(\.\d+)?)/i
                        },
                        android: {
                            re: function() {
                                return platforms.android || result.silk
                            },
                            ver: version,
                            os: /android[ \/-](\d+(\.\d+)*)/i
                        }
                    }
                ];

            // action

            browsers.forEach( function( level ) {
                Object.keys( level ).some( function( name ) {
                    var
                        browser = level[ name ],
                        re = browser.re,
                        ver = browser.ver,
                        os = browser.os;

                    // checking
                    if ( re instanceof Function ) re = re();
                    if ( re instanceof RegExp ) re = re.test( ua );
                    if ( !re ) return;
                    result[ name ] = true;

                    // version
                    if ( ver instanceof Function ) ver = ver();
                    if ( ver instanceof RegExp ) ver = first( ver );
                    result.ver || ( result.ver = parseFloat( ver ) || 0 );
                    result.major || ( result.major = parseInt( ver ) || 0 );

                    // os
                    if ( os instanceof Function ) os = os();
                    result.os || ( result.os = os );
                    return true;
                });
            });

            // mobile
            result.mobile = result.ios || result.android || result.likeAndroid;

            console.log( 'browser:', result );
            return result;
        }

        // API
        // browser.detect = detect;
        return browser;
    });