/**
 *
 * Objects
 * -------
 *
 * Helpers for object manipulations
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'objects',
    function() {
        'use strict';

        /**
         * Get text represented type of the instance
         * like 'Number', 'String', 'Object', 'Array', 'Arguments'
         * @param {*} instance
         * @return {string} instance type
         */
        function type( instance ) {
            var type = Object.prototype.toString.call( instance );
            return type.substr( 8, type.length - 9 );
        }

        /**
         * Dot notation (getter/setter)
         * @param object - origin object
         * @param notation - dot-notation query string
         * @param value - setter mode
         * @returns {*}
         */
        function dotted( object, notation, value ) {
            var
                dot = '.',
                setter = arguments.length == 3,
                args,
                key,
                current;

            // get property by key
            notation = 'string' == typeof notation
                ? notation.split( dot )
                : notation;
            
            if ( !notation.length ) return object;
            key = notation.shift();
            current = object[ key ];

            // property is absent
            if ( undefined === current )
                if ( !setter ) return;
                else if ( notation.length )
                    // create property
                    current = object[ key ] = {};

            // iterate
            if ( notation.length ) {
                args = [ current, notation ];
                if ( setter ) args.push( value );
                return dotted.apply( this, args );
            }
            // setter
            else if ( setter ) {
                current = object[ key ] = value;
            }
            // getter
            return current;
        }

        /**
         * Shallow object clone
         * @param object
         * @returns {undefined|object}
         */
        function clone( object ) {
            // clone array
            if ( 'Array' == type( object ))
                return [].concat( object );
            // clone object
            if ( 'object' != typeof object ) return;
            var res = {};
            for ( var key in object )
                if ( object.hasOwnProperty( key ))
                    res[ key ] = object[ key ];
            return res;
        }

        /**
         *
         * @param set
         * @param element
         * @returns {*}
         */
        function removeFromArray( set, element ) {
            if ( 'Array' != type( set )) return;
            if ( !set.length ) return;
            do {
                var index = set.indexOf( element );
                if ( !!~index ) set.splice( index, 1 );
            }
            // repeat until exists
            while ( !!~index );
            return set;
        }


        // API
        return window.Objects = {
            type: type,
            dotted: dotted,
            clone: clone,
            removeFromArray: removeFromArray
        };
    });