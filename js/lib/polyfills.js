/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Polyfills
 * ---------
 *
 */

define( 'polyfills',
    function() {

        /**
         * Object.keys
         */
        if ( !Object.keys )
            Object.keys = function( obj ) {
                var keys = [];
                if ( !obj ) return [];
                for ( var key in obj )
                    if ( obj.hasOwnProperty( key ))
                        keys.push( key );
                return keys;
            };

        /**
         * Function.bind
         */
        if ( !Function.prototype.bind ) {
            Function.prototype.bind = function( context ) {
                var
                    fn = this,
                    args = [].slice.call( arguments, 1 );
                return function() {
                    return fn.apply( context, args );
                };
            }
        }

        /**
         * requestAnimationFrame
         * cancelAnimationFrame
         * @author Erik Möller (Opera engineer)
         */
        ( function() {

            var lastTime = 0;
            var vendors = ['webkit', 'moz'];
            for ( var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x ) {
                window.requestAnimationFrame = window[ vendors[ x ] + 'RequestAnimationFrame' ];
                window.cancelAnimationFrame =
                    window[ vendors[ x ] + 'CancelAnimationFrame' ]
                    || window[ vendors[ x ] + 'CancelRequestAnimationFrame' ];
            }
            if ( !window.requestAnimationFrame )
                window.requestAnimationFrame = function( callback, element ) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max( 0, 16 - ( currTime - lastTime ));
                    var id = window.setTimeout(
                        function() { callback( currTime + timeToCall ); },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };
            if ( !window.cancelAnimationFrame )
                window.cancelAnimationFrame = function( id ) {
                    clearTimeout( id );
                };
        }() );


    });