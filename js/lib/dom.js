/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * DOM
 * ---
 *
 */

define( 'dom',
    function() {

        'use strict';

        /**
         * DOM nodes traversal
         * @param {Element} [parent] - optional parent node to search from
         * @param {string} selector - CSS selector string
         * @returns {Element[]} - list of the founded elements
         */
        function select( parent, selector ) {

            // optional args
            if ( arguments.length < 2 )
                var selector = parent,
                    parent = undefined;
            // args
            var parent = type( parent ) ? parent : null;
            // select dom elements
            var set = ( parent || document ).querySelectorAll( selector );
            // convert node list to array
            return set ? [].slice.call( set ) : [];
        }

        function first( parent, selector ) {
            return select.apply( null, arguments ).shift();
        }

        /**
         * Parse html to DOM elements
         * @param {string} content - HTML content
         * @returns {Element[]} - set of DOM elements or []
         */
        function html( content ) {
            var
                content = String( content || '' ),
                el = document.createElement( 'DIV' ),
                childs = [];

            // render html
            el.innerHTML = content;
            // get children
            if ( el.childNodes.length ) {
                childs = [].slice.call( el.childNodes );
                // extract children from the fake parent
                childs.forEach( function( child ) {
                    child.remove();
                });
            }
            // array of children
            return childs;
        }

        /**
         * Append the set of elements to the parent
         * @param {Element} parent - parent element
         * @param {Element[]} childs - set of elements
         * @returns {*|Array}
         */
        function append( parent, childs ) {
            // args
            var parent = type( parent ) ? parent : null,
                childs = childs && [].concat( childs );
            // checking
            if ( !parent ) return;
            if ( !childs || !childs.length ) return;
            // appending
            childs.forEach( function( el ) {
                if ( type( el ))
                    parent.appendChild( el );
            });
            return childs;
        }

        function attr( el, values ) {

        }

        /**
         * Remove element children
         * @param {Element} el
         */
        function empty( el ) {
            if ( !type( el )) return;
            // remove each child
            while ( el.hasChildNodes() )
                el.removeChild( el.lastChild );
        }

        /**
         * Set CSS styles of elements
         * @param {Element|Element[]} els - element (or set of elements)
         * @param {object} styles - css properties according with values
         */
        function css( els, styles ) {
            var
                els = els ? [].concat( els ) : false,
                properties = Object.keys( styles );

            if ( !els ) return;

            // assign css
            els.forEach( function( el ) {
                properties.forEach( function( prop ) {

                    // get correct css prop name
                    var
                        value = styles[ prop ],
                        correctProps = _normalizeCssProperty( prop );
                    // convert to an array
                    if ( correctProps )
                        correctProps = [].concat( correctProps );

                    // assign css values
                    correctProps.forEach( function( _prop ) {
                        el.style[ _prop ] = _normalizeCssValue( _prop, value );
                    });

                });
            });
        }


        // Helpers
        // -------

        /**
         * Helper: normalize css property name
         * automatically set browser prefixes
         * camel-case name
         *
         * TODO
         *
         * @param prop - css property name
         * @returns {*|*[]} Normalized property name (or set of names)
         * @private
         */
        function _normalizeCssProperty( prop ) {

            // camel-case
            prop = prop.split( '-' )
                .map( function( v ) {
                    return ( v || '' ).charAt( 0 ).toUpperCase() + v.slice( 1 );
                }).join( '' );
            prop = prop.charAt( 0 ).toLowerCase() + prop.slice( 1 );

            return prop;
        }

        /**
         * Helper: Normalize given value of the css property
         * for example: `width: 10 -> width: '10px'`
         *
         * TODO
         *
         * @param {string} prop - css property name
         * @param {*} value - css value
         * @returns {*} Normalized value
         * @private
         */
        function _normalizeCssValue( prop, value ) {
            switch ( prop ) {

                // pixels
                case 'width':
                case 'height':
                case 'top':
                case 'bottom':
                case 'left':
                case 'right':
                case 'marginLeft':
                case 'marginRight':

                    var found = String( value || '' ).match( /(\-?[\d\.]+)([\w\%]+)?/ ),
                        val = found && found.shift() && found.shift(),
                        unit = found && found.shift();
                    if ( !found ) return '';
                    return ( parseFloat( val ) || 0 ) + ( unit || 'px' );
            }
            return value;
        }

        /**
         * Detect type of the DOM Element
         * (or `undefined` if isn't DOM element)
         * @param el
         * @returns {*}
         */
        function type( el ) {
            if ( !el ) return;
            if ( 'undefined' == typeof el.nodeType ) return;
            var types = {
                1: 'undefined' != typeof Element ? Element : null,
                2: 'undefined' != typeof Attr ? Attr : null,
                3: 'undefined' != typeof Text ? Text : null,
                4: 'undefined' != typeof CDATASection ? CDATASection : null,
                5: 'undefined' != typeof EntityReference ? EntityReference : null,
                6: 'undefined' != typeof Entity ? Entity : null,
                7: 'undefined' != typeof ProcessingInstruction ? ProcessingInstruction : null,
                8: 'undefined' != typeof Comment ? Comment : null,
                9: 'undefined' != typeof Document ? Document : null,
                10: 'undefined' != typeof DocumentType ? DocumentType : null,
                11: 'undefined' != typeof DocumentFragment ? DocumentFragment : null,
                12: 'undefined' != typeof Notation ? Notation : null
            };
            for ( var type in types )
                if ( types.hasOwnProperty( type ))
                    // check element type
                    if ( '' + el.nodeType == type )
                        if ( types[ type ])
                            if ( el instanceof types[ type ])
                                return types[ type ];
                            // if browser didn't support that type
                            else return Object;
        }


        // API
        return window.d = {
            select: select,
            first: first,
            html: html,
            css: css,
            append: append,
            attr: attr,
            empty: empty,
            type: type
        }

    });