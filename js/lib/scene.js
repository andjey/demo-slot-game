/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene manager
 * -------------
 *
 * ( namespaced )
 *
 */

define( 'scene',
    [
        'lib/class',
        'lib/scene/layer',
        'lib/canvas',
        'lib/events',
        'lib/browser',
        'lib/namespace'
    ],
    function( Class, Layer, Canvas, Events, Browser, Namespace ) {

        'use strict';

        var t = 0, counter = 0;

        /**
         * Scene Class
         * -----------
         *
         * @type {Scene}
         */
        var Scene = Class({

            extend: Layer,

            constructor: function( parent, width, height, options ) {
                var self = this;

                // init super class
                this.super();


                // Properties
                // ----------

                // canvas
                this.canvas = new Canvas( parent, width, height, { use_buffer: true });

                // rendering
                this.need_to_update = false;
                this.request_id = null;
                this.render = this.render.bind( this );  // keep `this`

                // normalize options
                options = options || {};
                options.clear = undefined === options.clear ? false : !! options.clear;
                this.options = options;


                // Visual
                // ------

                this.background = options.background;
                if ( this.background )
                    this.canvas
                        .clear( this.background )
                        .drawBuffer();


                // Events
                // ------

                // Refresh the scene (by child)
                this.on( 'refresh', function() {
                    self.render( true );
                });

                // Canvas interaction (DOM)
                Events.on( this.canvas.canvas, 'click', this.onClick, this );
                Events.on( this.canvas.canvas, 'touchend', this.onClick, this );
                if ( !Browser.mobile )
                    Events.on( this.canvas.canvas, 'mousemove', this.onMove, this );

            },


            // Properties
            // ----------

            canvas: null,
            options: null,
            background: null,

            // render
            need_to_update: null,  // render condition flag
            request_id: null,  // request animation frame id


            // Control
            // ------------------------------------------------------------------------------------------------------

            /**
             * Destroy the scene
             */
            remove: function() {
                // is this need?
            },

            refresh: function() {
                this.need_to_update = true;
                this.render();
            },

            /**
             * Stop rendering frames
             * @returns {Scene}
             */
            stopRender: function() {
                // break render loop
                this.request_id = cancelAnimationFrame( this.request_id );
                // reset flag
                this.need_to_update = false;
                // chain
                return this;
            },

            /**
             * Start render loop
             * @param {boolean} [force]
             * @returns {Scene}
             */
            render: function( force ) {
                // stat
                // if ( t > + new Date ) counter ++;
                // else document.title = 'fps: ' + counter, counter = 0, t = + new Date + 1000;

                // render condition
                if ( true !== force && !this.need_to_update ) return this;
                // reset flag
                this.need_to_update = false;
                // stop excess renders
                if ( true === force && this.request_id ) this.stopRender();
                // request next frame render
                this.request_id = requestAnimationFrame( this.render );
                // redraw the frame
                this.renderFrame();

                return this;
            },


            // Rendering
            // ------------------------------------------------------------------------------------------------------

            /**
             * Render frame
             */
            renderFrame: function() {


//              OPTIMIZATION
//              ------------
//
//                // calculate animations
//                this.animationFrame();
//                // prepare frame
//                this.prepareFrame();
//                // fill frame content
//                this.componentFrame();
//                // draw buffer if need
//                this.canvas.drawBuffer();
//            },
//
//            /**
//             * Calculate animations
//             */
//            animationFrame: function() {
//                this.frameChildren({
//                    action: function( child ) {
//                        if ( child.in_process
//                            && !child.in_pause
//                            && child.renderAnimationFrame() )
//                            this.need_to_update = true;
//                    }
//                });
//            },
//
//            /**
//             * Prepare scene frame
//             */
//            prepareFrame: function() {
//                // clear frame
//                if ( this.options.clear )
//                    this.canvas.clear( this.background );
//            },
//
//            /**
//             * Show scene components
//             */
//            componentFrame: function() {

                // prepare frame: clear frame
                if ( this.options.clear )
                    this.canvas.clear( this.background );

                // Calculate animations
                // Show scene components
                this.frameChildren({
                    layer: function( layer ) { this.canvas.zeroXY( layer.x, layer.y ); },
                    // Calculate animations
                    action: function( child ) {
                        if ( child.in_process && !child.in_pause && child.renderAnimationFrame() ) this.need_to_update = true;
                    },
                    // Show scene components
                    child: function( child ) { child.render && child.render( this.canvas ); },
                    after: function( layer ) { this.canvas.releaseXY(); }
                });

                // draw buffer if need
                this.canvas.drawBuffer();
            },


            // Events
            // ------------------------------------------------------------------------------------------------------

            onClick: function( e ) {
                var
                    self = this,
                    x = ( e.pageX - self.canvas.canvas.offsetLeft ) / this.canvas.scale_x,
                    y = ( e.pageY - self.canvas.canvas.offsetTop ) / this.canvas.scale_y,
                    zero_x = 0, zero_y = 0;

                self.frameChildren({
                    layer: function( layer ) {
                        zero_x += layer.x;
                        zero_y += layer.y;
                    },
                    child: function( child ) {
                        if ( child._listeners[ 'click' ]
                            && child.x + zero_x  < x
                            && child.y + zero_y < y
                            && x < child.x + zero_x + child.width
                            && y < child.y + zero_y + child.height
                            && ( Browser.mobile || ( !child.alpha_level || child.intersect( x - child.x - zero_x, y - child.y - zero_y )) ))
                        {
                            setTimeout( function() {
                                child.emit( 'click', x, y, 100 );
                            }, 0 );
                        }
                    },
                    after: function( layer ) {
                        zero_x -= layer.x;
                        zero_y -= layer.y;
                    }
                });
            },

            onMove: function( e ) {
                var
                    self = this,
                    x = ( e.pageX - self.canvas.canvas.offsetLeft ) / this.canvas.scale_x,
                    y = ( e.pageY - self.canvas.canvas.offsetTop ) / this.canvas.scale_y,
                    zero_x = 0, zero_y = 0;

                self.frameChildren({
                    layer: function( layer ) {
                        zero_x += layer.x;
                        zero_y += layer.y;
                    },
                    child: function( child ) {

                        // check intersection
                        if ( child.x + zero_x < x
                            && child.y + zero_y < y
                            && x < child.x + zero_x + child.width
                            && y < child.y + zero_y + child.height
                            && ( child._listeners.over || child._listeners.move )
                            && ( Browser.mobile || ( !child.alpha_level || child.intersect( x - child.x - zero_x, y - child.y - zero_y )) ))
                        {
                            // event: over
                            if ( child._listeners.over )
                                if ( !child._event_is_over ) {
                                    child._event_is_over = true;
                                    child.emit( 'over', x, y );
                                }
                            // event: mouse move
                            if ( child._listeners.move )
                                child.emit( 'move', x, y );
                        }
                        // out of object
                        else {
                            // event: out
                            if ( child._event_is_over ) {
                                child._event_is_over = false;
                                child.emit( 'out', x, y );
                            }
                        }
                    },
                    after: function( layer ) {
                        zero_x -= layer.x;
                        zero_y -= layer.y;
                    }
                });
            },


            // Helpers
            // ------------------------------------------------------------------------------------------------------

            frameChildren: function( params ) {

                var self = this;
                params = params || {};

                // each of children
                function iterate( layer ) {
                    var child;
                    // except hidden
                    if ( !layer.visible ) return;
                    // call before
                    params.before && params.before.call( self, layer );
                    // call action on layer
                    params.action && params.action.call( self, layer );
                    params.layer && params.layer.call( self, layer );
                    // show each of children
                    for ( var i = 0; i < layer.children.length; i++ ) {
                        child = layer.children[ i ];
                        if ( !child.visible ) continue;
                        // show layer
                        if ( child instanceof Layer ) iterate( child );
                        // call action
                        else {
                            params.action && params.action.call( self, child );
                            params.child && params.child.call( self, child );
                        }
                    }
                    // call after
                    params.after && params.after.call( self, layer );
                }
                iterate( self );
                // chain
                return self;
            },

            eventChildren: function( params ) {

            }

        });


        // API
        // ----------------------------------------------------------------------------------------------------------

        var api = Namespace(
            function( space, parent, width, height, options ) {
                return new Scene( parent, width, height, options );
            });
        api.Class = Scene;
        return api;

    });