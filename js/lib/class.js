/**
 *
 * Objects
 * -------
 *
 * Helpers for object manipulations
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'class',
    [
        'objects'
    ],
    function( Objects ) {
        'use strict';

        function Class( definition ) {
            var
                definition = definition || {},
                keys = 'object' == typeof definition ? Object.keys( definition ) : [],
                constructor = 'function' == typeof definition.constructor ? definition.constructor : function(){},
                extend = 'function' == typeof definition.extend ? definition.extend : false,

                /**
                  * Constructor
                  * @constructor
                  */
                _class = function(){
                    // super class
                    this.superclass = extend; // && extend.prototype;
                    this.super = _super;
                    // original constructor
                    constructor && constructor.apply( this, arguments );
                };

            // extend class
            if ( extend ) inherits( _class, extend );

            // fill prototype
            keys.forEach( function( key ) {
                _class.prototype[ key ] = definition[ key ];
            });

            return _class;
        }

        /**
         * Helper: Call superclass constructor
         * @params {arguments|...*} arguments to call with
         */
        function _super() {
            var args = arguments;
            if ( 1 == args.length
                && 'Arguments' == Objects.type( args[ 0 ]) )
                args = args[ 0 ];

            // call superclass
            if ( this.superclass )
                this.superclass.apply( this, args );

//            if ( this.superclass
//                && this.superclass.constructor )
//                this.superclass.constructor.apply( this, args );
        }

        /**
         * Prototype inheritance
         * @param {function} child - class
         * @param {function} parent - super class
         */
        function inherits( child, parent ) {
            var F = function() {};
            F.prototype = parent.prototype;
            child.prototype = new F();
            child.prototype.constructor = child;
        }


        // API
        // ---

        Class.inherits = inherits;
        return Class;

    });