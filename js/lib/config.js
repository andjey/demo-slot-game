
define( 'config',
    [
        'lib/namespace',
        'lib/model'
    ],
    function( Namespace, Model ) {

        return Namespace( function( space ) {
            return new Model();
        });

    });