/**
 *
 * Model
 * -----
 *
 * Getter, setter, events, dot notation
 *
 * ### Events
 *
 *      reset ( model, oldContent )
 *      empty ( oldContent )
 *      change ( propertyName, newValue, oldValue )
 *      change:propertyName ( newValue, oldValue )
 *
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'model',
    [
        'lib/class',
        'lib/objects',
        'lib/emitter'
    ],
    function( Class, Objects, Emitter ) {

        var Model = Class({

            extend: Emitter,

            /**
             * Model
             * @param content
             * @constructor
             */
            constructor: function( content ) {
                // init super class
                this.super();
                // properties
                this._values = content || {};
            },

            /**
             * Model content storage
             * @type {object}
             */
            _value: undefined,

            /**
             * Reset model content
             * @param {object} values - new model content
             * @returns {Model} chain
             */
            reset: function( values ) {
                if ( 'Object' != Objects.type( values )
                    && 'Array' != Objects.type( values ))
                    return this;
                var old = this._values;
                this._values = values;
                this.emit( 'reset', this, old );
                return this;
            },

            /**
             * Clear model content
             * @returns {Model} chain
             */
            empty: function() {
                var old = this._values;
                this._values = {};
                this.emit( 'empty', old );
                return this;
            },

            /**
             * Set property
             * @param {string} name - property key path (dot-notation)
             * @param {*} value
             * @returns {Model} chain
             */
            set: function( name, value ) {
                if ( arguments.length < 2 ) return this;
                // updating
                var old = Objects.dotted( this._values, name ),
                    last = Objects.dotted( this._values, name, value );
                // update event
                if ( old !== last ) {
                    this.emit( 'update', name, value, old );
                    this.emit( 'update:' + name, value, old );
                }
                return this;
            },

            // getters

            /**
             * Get property
             * @param {string|string[]} name - property key path (dot-notation)
             * @returns {*}
             */
            get: function( name ) {
                return Objects.dotted( this._values, name );
            },

            /**
             * Get model content as single object
             * @returns {object}
             */
            toObject: function() {
                return Objects.clone( this._values );
            }
        });


        // API
        // ---

        return Model;

    });