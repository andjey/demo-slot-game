/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component
 * ---------------
 *
 */

define( 'canvas',
    [
        'lib/class',
        'lib/emitter',
        'lib/polyfills'
    ],
    function( Class, Emitter ) {

        'use strict';

        var Canvas = Class({

            extend: Emitter,

            constructor: function( parent_element, width, height, options ) {

                // optional args
                if ( + parent_element )
                    var options = height,
                        height = width,
                        width = parent_element,
                        parent_element = undefined;
                if ( 'object' == width )
                    var options = width,
                        height = undefined,
                        width = undefined;
                if ( 'object' == height )
                    var options = height,
                        height = undefined;

                var opts = options || {};

                // init super    class
                this.super();

                // normalize options
                opts.id = '' + ( opts.id || '' );
                opts.use_buffer = undefined === opts.use_buffer ? false : !! opts.use_buffer;
                this.options = opts;

                // setup
                var parent = parent_element && document.querySelector( parent_element );
                this.X = + opts.X || 0;
                this.Y = + opts.Y || 0;
                this.stackXY = [];
                this.width = + width || + opts.width || 0;
                this.height = + height || + opts.height || 0;
                this.scale_x = + opts.scale_x || 1;
                this.scale_y = + opts.scale_y || 1;
                this.translate_x = + opts.translate_x || 0;
                this.translate_y = + opts.translate_y || 0;

                // optimization
                this.optimize = opts.optimize || false;

                // init
                var canvas = document.createElement( 'canvas' );
                canvas.width = this.width;
                canvas.height = this.height;
                if ( opts.id ) canvas.setAttribute( 'id', opts.id );

                // canvas
                this.canvas = canvas;
                this.context = canvas.getContext( '2d' );
                if ( parent ) parent.appendChild( canvas );

                // buffer
                if ( opts.use_buffer ) {
                    var buffer = document.createElement( 'canvas' );
                    buffer.width = canvas.width;
                    buffer.height = canvas.height;
                    this.buffer = buffer;
                    this.buffer_context = buffer.getContext( '2d' );
                }
            },

            // canvas
            canvas: null,
            context: null,

            // buffer
            buffer: null,
            buffer_context: null,

            parent: null,
            optimize: null,
            width: null,
            height: null,

            // Custom translation (fast)
            X: null,
            Y: null,
            stackXY: null,

            scale_x: null,
            scale_y: null,
            translate_x: null,
            translate_y: null,


            // Base API only
            // -------------
            // functions which working with native canvas

            fillStyle: '#fff',
            textAlign: 'left',
            font: '12px Helvetica, Sans-serif',

            save: function() {
                this.getContext().save();
                return this;
            },
            restore: function() {
                this.getContext().restore();
                return this;
            },
            translate: function( x, y ) {
                this.translate_x += x;
                this.translate_y += y;
                this.getContext().translate( x, y );
                return this;
            },

            scale: function( scale_x, scale_y ) {
                var
                    context = this.getContext(),
                    scale_x = + scale_x || 1,
                    scale_y = + scale_y || 1;
                this.scale_x *= scale_x ;
                this.scale_y *= scale_y;
                context.scale( scale_x, scale_y );
                return this;
            },

            fillRect: function( x, y, width, height ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                context.fillStyle = this.fillStyle;
                context.fillRect.apply( context, this.intArgs( arguments ));
                return this;
            },

            clearRect: function( x, y, width, height ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                context.clearRect.apply( context, this.intArgs( arguments ));
                return this;
            },

            drawImage: function( image, sx, sy, s_width, s_height, x, y, width, height ) {
                var context = this.getContext();
                arguments[ 0 ] = image instanceof Canvas ? image.canvas : image;
                if ( arguments.length < 6 ) {
                    arguments[ 1 ] += this.X;  // real arg (6): x
                    arguments[ 2 ] += this.Y;  // real arg (7): y
                }
                context.drawImage.apply( context, this.intArgs( arguments, 1 ));
                return this;
            },

            fillText: function( text, x, y, width ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                context.fillStyle = this.fillStyle;
                context.font = this.font;
                context.textAlign = this.textAlign;
                context.fillText.apply( context, this.intArgs( arguments, 1 ));
                return this;
            },

            measureText: function( text ) {
                var context = this.getContext();
                context.fillStyle = this.fillStyle;
                context.font = this.font;
                return context.measureText.apply( context, arguments );
            },

            getImageData: function( x, y, width, height ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                return context.getImageData.apply( context, arguments );
            },

            putImageData: function( pixels, x, y, dx, xy, dw, dh ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                return context.putImageData.apply( context, arguments );
            },

            // ---

            line: function( x, y, end_x, end_y, line_width, color, stroke ) {
                var context = this.getContext();
                x += this.X;
                y += this.Y;
                if ( line_width ) context.lineWidth = + line_width;
                if ( color ) context.strokeStyle = color;
                if ( stroke ) context.lineCap = stroke;
                context.beginPath();
                context.moveTo( + x || 0, + y || 0 );
                context.lineTo( + end_x, + end_y );
                context.stroke();
                return this;
            },


            // Buffer
            // ------

            /**
             * Copy drawing from the buffer to the context
             * @returns {Canvas}
             */
            drawBuffer: function() {
                if ( this.options.use_buffer )
                    this.context.drawImage( this.buffer, 0, 0 );
                return this;
            },

            /**
             * Get active context
             * (of the canvas or of the buffer)
             * @returns {CanvasRenderingContext2D}
             */
            getCanvas: function() {
                return this.options.use_buffer ? this.buffer : this.canvas;
            },

            /**
             * Get active context
             * (of the canvas or of the buffer)
             * @returns {CanvasRenderingContext2D}
             */
            getContext: function() {
                return this.options.use_buffer ? this.buffer_context : this.context;
            },


            // Helpers
            // -------

            clear: function( color ) {
                var canvas = this.getCanvas();
                if ( !color )
                    this.clearRect( 0, 0, canvas.width, canvas.height );
                else {
                    this.fillStyle = color;
                    this.fillRect( 0, 0, canvas.width, canvas.height );
                }
                return this;
            },

            // Resize canvas

            resize: function( width, height ) {
                this.setWidth( width );
                this.setHeight( height );
                return this;
            },
            setWidth: function( value ) {
                var value = + value || false;
                if ( false === value ) return this;
                this.width = value;
                this.canvas.width = value;
                if ( this.buffer ) this.buffer.width = value;
                return this;
            },
            setHeight: function( value ) {
                var value = + value || false;
                if ( false === value ) return this;
                this.height = value;
                this.canvas.height = value;
                if ( this.buffer ) this.buffer.height = value;
                return this;
            },

            // Transformations

            zoom: function( zoom_x, zoom_y ) {
                return this.scale( zoom_x / this.scale_x, zoom_y / this.scale_y );
            },

            offset: function( offset_x, offset_y ) {
                return this.translate( this.translate_x - offset_x, this.translate_y - offset_y );
            },

            // Zero coordinates

            // setup
            zeroXY: function( offset_x, offset_y ) {
                var xy = { x: this.X, y: this.Y };
                this.stackXY.push( xy );
                this.X += offset_x || 0;
                this.Y += offset_y || 0;
                return this;
            },
            // release
            releaseXY: function() {
                var xy = this.stackXY.pop();
                if ( !xy ) this.X = 0, this.Y = 0;
                else this.X = xy.x, this.Y = xy.y;
            },


            // Image
            // -----

            /**
             * Fill region by image pattern
             * @param {Image} image
             * @param {number} x
             * @param {number} y
             * @param {number} width
             * @param {number} height
             * @param {string} [repeat=repeat] repeat|repeat-x|repeat-y|no-repeat
             */
            fillImage: function( image, x, y, width, height, repeat ) {
                var
                    canvas = this.getCanvas(),
                    context = this.getContext(),
                    width = + width || canvas.width - x,
                    height = + height || canvas.height - y,
                    w = image.width,
                    h = image.height,
                    ox, oy,
                    crop_w, crop_h;

                // fill area with image
                for ( oy = 0; oy < height; oy += h )
                    for ( ox = 0; ox < width; ox += w ) {
                        crop_w = ox + w < width ? w : width - ox;
                        crop_h = oy + h < height ? h : height - oy;
                        this.drawImage( image, 0, 0, crop_w, crop_h, ox + x, oy + y, crop_w, crop_h );
                    }
                return this;
            },


            // Text
            // ----

            // based on [](http://habrahabr.ru/post/140235/)

            /**
             * Split text to lines by width
             * @param {string} text
             * @param {number} x
             * @param {number} y
             * @param {object} params
             * @returns {Canvas}
             */
            wrapText: function( text, x, y, params ) {
                var
                    // args
                    text = '' + ( text || '' ),
                    x = + x || 0,
                    y = + y || 0,
                    params = params || {},

                    // params
                    max_width = + params.max_width,
                    line_height = + params.line_height || this.fontHeight(),
                    width = + params.width || false,
                    nowrap = undefined === params.nowrap ? false : !! params.nowrap,
                    resize = !! params.resize,
                    canvas = params.canvas || this.getCanvas(),
                    context = params.context || this.getContext(),
                    // line height offset, 20% by default
                    baseline = + params.baseline || + ( line_height * 0.20 ),

                    // locals
                    self = this,
                    any_space = /\s+/,
                    words = text.split( any_space ),
                    box_width = 0,
                    box_height = y,
                    line = nowrap ? text : '',
                    lines = [];

                // checking
                if ( !text ) return self;
                // check context and canvas
                if ( !canvas || !context ) return self;
                if ( canvas
                    && !( canvas instanceof Canvas ))
                    resize = false;  // reset flag
                if ( context
                    && '[object CanvasRenderingContext2D]' != Object.prototype.toString.call( context ))
                    return self;
                // bad width value
                if ( !resize
                    && ( !max_width || max_width > canvas.width - x ))
                    max_width = canvas.width - x;
                // just one word
                if ( !words.length )
                    // split by letters
                    words = text.split( '' );

                // split lines by max width
                !nowrap &&
                words.forEach( function( word ) {
                    var space = ( line.length ? ' ' : '' ),
                        line_width = canvas.measureText( line + space + word ).width;
                    // split line
                    if ( line && line_width > max_width ) {
                        // resize width
                        box_width = Math.max( box_width, canvas.measureText( line ).width );
                        // resize height
                        box_height += line_height;
                        // keep line
                        lines.push( line );
                        line = word;
                    } else {
                        line += space + word;
                    }
                });

                // last or single line
                // resize width
                box_width = Math.max( box_width, canvas.measureText( line ).width );
                // resize height
                box_height += line_height;
                // keep last line
                lines.push( line );

                // resize canvas if need
                if ( resize ) {
                    // increase canvas width if need
                    if ( box_width > canvas.width )
                        canvas.setWidth( box_width );
                    // increase canvas height if need
                    if ( box_height > canvas.height )
                        canvas.setHeight( box_height );
                }

                // prepare dimension
                y += line_height;

                // show lines
                lines.forEach( function( line ) {
                    // show text without last word (`width` it's optional argument)
                    canvas.fillText.apply( self, [ line, x, y - baseline ].concat( width || [] ));
                    // prepare next line
                    y += line_height;
                });
                return self;
            },

            /**
             * Determinate full height of the font
             * @param {string} [font] like '5px Arial'
             * @returns {number} font height in pixels
             */
            fontHeight: function( font ) {
                var
                    font = font || this.font,
                    element = document.createElement( 'span' ),
                    height;
                element.style.cssText = '\
                    font: '+ font +';\
                    white-space: nowrap;\
                    display: inline;';
                element.appendChild( document.createTextNode( 'string' ));
                document.body.appendChild( element );
                height = element.offsetHeight;
                return ( document.body.removeChild( element )), height;
            },


            // Effects
            // -------

            /**
             *
             * @param {string} type Effect name: 'grayscale'
             * @param {number} [x=0]
             * @param {number} [y=0]
             * @param {number} width
             * @param {number} height
             * @param {object} [params]
             * @returns {Canvas}
             */
            effect: function( type, x, y, width, height, params ) {

                // optional args
                if ( arguments.length < 5 )
                    var params = width,
                        height = y,
                        width = x,
                        x = y = undefined;

                var
                    params = params || {},
                    type = '' + ( type || '' ),
                    x = + x || 0,
                    y = + y || 0,
                    width = + width || 0,
                    height = + height || 0,

                    // params
                    canvas = params.canvas || this,

                    // process
                    fx = type && this[ 'fx' + type[ 0 ].toUpperCase() + type.substr( 1 )],
                    pixels,
                    ox = 0,
                    oy = 0,
                    offset,
                    rgb;

                // checking
                if ( !fx || !width || !height ) return this;
                if (!( canvas instanceof Canvas )) return this;

                // get image data
                pixels = canvas.getImageData( x, y, width, height );

                // process each pixel
                for ( ; oy < pixels.height; oy++ ) {
                    var z = 0;
                    for( ox = 0; ox < pixels.width; ox++ ) {
                        offset = oy * 4 * pixels.width + ox * 4;
                        rgb = fx( pixels.data[ offset ], pixels.data[ offset + 1 ], pixels.data[ offset + 2 ], ox, oy, pixels );
                        pixels.data[ offset ] = rgb[ 0 ];
                        pixels.data[ offset + 1 ] = rgb[ 1 ];
                        pixels.data[ offset + 2 ] = rgb[ 2 ];
                    }
                }
                // update canvas
                canvas.putImageData( pixels, x, y, 0, 0, width, height );
                return this;
            },

            fxGrayscale: function( r, g, b, x, y, pixels ) {
                var gray = ( r + g + b ) / 3;
                return [ gray, gray, gray ];
            },

            fxInversion: function( r, g, b, x, y, pixels ) {
                return [ 255 - r, 255 - g, 255 - b ];
            },


            // Optimization
            // ------------

            /**
             * Optimisation: Parse integers from float arguments
             * @param {Arguments} args
             * @param {int} skip
             * @returns {*[]}
             */
            intArgs: function( args, skip ) {
                if ( !this.optimize ) return args;
                // parse int
                if ( '[object Arguments]' != Object.prototype.toString.call( args )) return [];
                for ( var i = + skip || 0; i < args.length; i ++ )
                    args[ i ] = args[ i ] >> 0 || 0;  // parse int
                return args;
            }

        });


        // API
        // ---

        return Canvas;
    });