/**
 *
 * Finite State Machine
 * --------------------
 *
 * ( namespaced )
 *
 * ### Events
 *
 *      state ( current, [previous] ) - when state was changed
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'states',
    [
        'lib/class',
        'lib/emitter',
        'lib/objects',
        'lib/namespace'
    ],
    function( Class, Emitter, Objects, Namespace ) {

        'use strict';

        /**
         * State Machine Class
         */
        var StateMachine = Class({

            extend: Emitter,

            constructor: function() {
                this.super();

                // properties
                this._states = {};
                this._events = {};
                this._current = null;
            },

            _states: undefined,
            _events: undefined,
            _current: undefined,

            /**
             * Register new state
             * (or update exists)
             * @param {string} name
             * @param {object} [params]
             * @param {function} [enter]
             * @param {function} [leave]
             * @returns {StateMachine}
             */
            state: function( name, params, enter, leave ) {

                // optional args
                if ( 'function' == typeof params )
                    var leave = enter,
                        enter = params,
                        params = undefined;
                // args
                var
                    name = String( name || '' ),
                    params = params || {};

                if ( !name ) return this;
                // params
                enter = params.enter || enter;
                leave = params.leave || leave;
                enter = 'function' == typeof enter ? enter : false;
                leave = 'function' == typeof leave ? leave : false;

                // set state
                this._states[ name ] = {
                    enter: 'function' == typeof enter ? enter : false,
                    leave: 'function' == typeof leave ? leave : false
                };
                // initial
                if ( params.initial ) {
                    this._current = name;
                    if ( enter ) enter();
                    this.emit( 'state', name, null );
                }
                // chain
                return this;
            },

            /**
             * Unregister state
             * @param {string} name
             * @returns {StateMachine}
             */
            removeState: function( state ) {
                var self = this;

                // remove state
                delete self._states[ state ];

                // remove event links
                Object.keys( self._events ).forEach(
                    function( event ) {
                        var links = self._events[ event ];
                        Object.keys( links ).forEach(
                            function( name ) {
                                if ( state == name  // as previous
                                    || state == links[ name ])  // as next
                                    delete links[ name ];
                            }
                        );
                    }
                );
                // chain
                return self;
            },

            /**
             * Assign state combination to event
             * @param {string} name - event name
             * @param {string} previous - previous state name
             * @param {string} next - next state name
             * @returns {StateMachine}
             */
            event: function( name, previous, next ) {

                // walk array arguments
                var self = this;
                if ( 'Array' == Objects.type( previous )) {
                    previous.forEach( function( previous ) {
                        self.event( name, previous, next );
                    });
                    return self;
                }
                var
                    name = String( name || '' ),
                    event = self._events[ name ];

                // action
                if ( !name ) return self;
                if ( !event ) event = self._events[ name ] = {};
                event[ previous ] = next;

                // extend this
                self[ name ] = self.run.bind( self, name );

                // chain
                return self;
            },

            /**
             * Remove event link
             * @param {string} event
             * @param {string} current - state name
             * @param {string} previous - state name
             * @returns {StateMachine}
             */
            removeEvent: function( event, current, previous ) {
                var
                    links = this._events[ event ];
                if ( previous && current )
                    if ( links[ current ])
                        delete links[ current ][ previous ];
                    else if ( current )
                        delete links[ current ];
                    else if ( links )
                        delete this._events[ event ];
                return this;
            },

            /**
             * Set given state as current
             * @param {string} state
             */
            setState: function( state ) {
                var
                    // previous state by current state name
                    prevState = this._current,
                    previous = this._states[ prevState ],
                    // new current state
                    state = String( state || '' ),
                    current = this._states[ state ],
                    args = [].slice.call( arguments, 1 );

                if ( !current ) return this;

                // leave previous
                if ( previous )
                    previous.leave
                    && previous.leave();

                // enter new state
                this._current = state;
                current.enter
                && current.enter.apply( null, args );

                // event
                this.emit( 'state', state, prevState );
                // chain
                return this;
            },

            /**
             * Force current state changing
             * @param {string} event
             * @returns {StateMachine}
             */
            run: function( event ) {
                var
                    // links by event
                    links = this._events[ event ],
                    args = [].slice.call( arguments, 1),

                    // next state by event linkage
                    nextState = links && links[ this._current ];

                if ( !links || !nextState )
                    return this;
                // action
                this.setState.apply( this,
                    [ nextState ].concat( args ));
                // chain
                return this;
            },

            /**
             * Close state machine
             * Leave current state
             */
            end: function() {
                var
                    // previous state by current state name
                    prevState = this._current,
                    previous = this._states[ prevState ];

                // leave previous
                if ( previous )
                    previous.leave
                    && previous.leave();

                // event
                this.emit( 'end', prevState );
                // chain
                return this;
            },

            /**
             * Remove state machine
             * Empty states and events
             */
            remove: function() {
                // TODO
                // remove all events
                this.off()
            }

        });



        // API
        // ---

        var api = Namespace( function( space ) {
            return new StateMachine();
        });
        api.Class = StateMachine;
        return api;

    });