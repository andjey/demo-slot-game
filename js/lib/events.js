/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Browser Events
 * --------------
 *
 * List of events obtained from <https://github.com/fat/bean>
 */

define( 'events',
    [
        'lib/dom'
    ],
    function( Dom ) {

        'use strict';

        var
            _listeners = {};


        // Listeners
        // ---------

        /**
         * Listen the event
         * @param {Element} element
         * @param {string} type
         * @param {function} handler
         * @param {boolean} [capture]
         * @returns {boolean}
         */
        function on( element, type, handler, capture, context ) {
            if ( 'boolean' != typeof context )
                context = capture,
                capture = undefined;
            var
                self = this,
                handler = 'function' == typeof handler ? handler : false,
                bounded;

            if ( !handler ) return false;

            // prepare containers
            if ( !_listeners[ type ])
                _listeners[ type ] = [];

            // override handler
            bounded = function( e ) {
                var args = [].slice.call( arguments, 1 );
                args.unshift( event.call( self, e, element ));
                handler.apply( context || element, args );
            };

            // attach event
            addEvent( element, type, bounded, capture );

            // add to the set
            _listeners[ type ].push({
                element: element,
                capture: capture,
                handler: handler,
                listener: bounded
            });

            // return listener
            return bounded;
        }

        /**
         * Stop listen the event
         * Any arguments are optional.
         * You can miss any of the arguments to expand the set of events to remove.
         * @param {Element} [element] - DOM element
         * @param {string|string[]} [type] - type of the event or array of the types
         * @param {function} [handler] - original listener function
         * @param {boolean} [capture] - use capture (flag)
         * @returns {*}
         */
        function off( element, type, handler, capture ) {
            var
                v,
                len,
                index,
                types = ( type && [].concat( type ))
                    || Object.keys( _listeners );

            // remove all matching events
            types.forEach( function( t ) {

                // walk each of events of the given types
                for ( index = 0;
                      index < _listeners[ t ].length;
                      index ++ ) {

                    v = _listeners[ t ][ index ];

                    // remove matched events
                    if (( !element || v.element === element )
                        && ( !handler || v.handler === handler )
                        && ( undefined === capture || v.capture === capture ))
                    {
                        // detach event
                        removeEvent( v.element, t, v.listener, v.capture );
                        // remove record from the set
                        // node: shift index, because of slice
                        _listeners[ t ].splice( index--, 1 );
                    }
                }
            });
            // clean empty
            types.forEach( function( t ) {
                if ( _listeners[ t ]
                    && !_listeners[ t ].length )
                    delete  _listeners[ t ];
            });
        }

        /**
         * Extend event object
         * @param e
         * @returns {*|{}}
         */
        function event( e, element ) {
            e = e || window.event || {};

            // target to event element
            e.element = e.target ? e.target : e.srcElement;

            // stop the event
            e.stop = function() {
                if ( e.stopPropagation ) {
                    e.preventDefault();
                    e.stopPropagation();
                } else {
                    e.cancelBubble = true;
                    e.returnValue = false;
                }
                return false;
            };

            // coordinates (ie8)
            if ( null == e.pageX && null != e.clientX ) {
                var html = document.documentElement,
                    body = document.body;
                e.pageX = ( e.clientX + html.scrollLeft || body && body.scrollLeft || 0 ) - ( html.clientLeft || 0 );
                e.pageY = ( e.clientY + html.scrollTop || body && body.scrollTop || 0 ) - ( html.clientTop || 0 );
            }

            return e;
        }


        // Events (DOM)
        // ------------

        function addEvent( element, type, listener, capture ) {
            // w3c
            if ( element.addEventListener ) {
                element.addEventListener( type, listener, capture );
            }
            else
            // microsoft
            if ( element.attachEvent ) {
                element.attachEvent( 'on' + type, listener );
            }
            // custom only (not dom)
            return listener;
        }

        function removeEvent( element, type, listener, capture ) {
            // w3c
            if ( element.removeEventListener ) {
                element.removeEventListener( type, listener, capture );
                return true;
            }
            else
            // microsoft
            if ( element.detachEvent ) {
                return element.detachEvent( 'on' + type, listener );
            }
            // custom only (not dom)
            return false;
        }


        // API
        // ---

        return window.e = {
            on: on,
            off: off,
            // raw
            addEvent: addEvent,
            removeEvent: removeEvent
        };

    });
