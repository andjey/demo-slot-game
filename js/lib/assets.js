/**
 *
 * Asset manager
 * -------------
 *
 * ( namespaced )
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'assets',
    [
        'lib/class',
        'lib/objects',
        'lib/dom',
        'lib/namespace'
    ],
    function( Class, Objects, Dom, Namespace ) {

        'use strict';

        var Assets = Class({

            /**
             * Assets Manager Class
             * @constructor
             */
            constructor: function() {
                this.super( arguments );
                this._storage = {};
            },


            _storage: undefined,

            /**
             * Load group of assets
             * @param {object[]|object} config
             * @param {function} progress
             * @param {function} callback
             */
            loadGroup: function( config, progress, callback ) {
                var
                    self = this,
                    actions;

                // optional args
                if ( arguments.length < 3 )
                    callback = progress,
                    progress = undefined;

                // asynchronous loading of each asset

                // config as {name:{}}
                if ( 'Object' == Objects.type( config ))
                    actions = Object.keys( config )
                        .map( function( name ) {
                            return self.load.bind( self,
                                name, config[ name ]);
                        });
                else
                // config as [{name}]
                if ( 'Array' == Objects.type( config ))
                    actions = config.map( function( config ) {
                        return self.load.bind( self,
                            config.name, config );
                    });

                // begin loading
                async( actions, progress, callback );
            },

            /**
             * Load and init asset only
             * @param {string} [name]
             * @param {object} config
             * @param {function} [callback]
             */
            load: function( name, config, callback ) {

                // optional args
                if ( arguments.length < 3 )
                    callback = config,
                    config = name,
                    name = '';
                var
                    self = this,
                    config = config || {},
                    type = ( config.type || '' ).toLowerCase(),
                    _callback = 'function' == typeof callback ? callback : function(){},
                    url = config.url,

                    // override callback
                    callback = function( err, res ) {
                        // keep the instance
                        if ( name ) self._storage[ name ] = res;
                        // then callback origin
                        _callback.apply( null, arguments );
                    };

                if ( !url ) _callback( new Error( 'Incorrect URL' ));

                // assets
                switch( type ) {

                    // ------------------------------------------------------------- Sprite
                    case 'image':

                        // image source
                        var image = new Image();
                        image.src = url;
                        // complete
                        image.onload = function( e ) {
                            callback( null, image );
                        };
                        break;

                    // ------------------------------------------------------------- Sound
                    case 'sound':

                        // create audio
                        var audio = new Audio();
                        audio.src = url;
                        // on complete
                        audio.addEventListener( 'canplaythrough', function( e ) {
                            callback( null, audio );
                        });

                        // TODO: ios audio
                        // audio = document.createElement('audio');
                        // audio.autoplay = false;
                        // audio.preload = false;
                        // audio.addEventListener( 'canplaythrough', onload, false );
                        // document.getElementsByTagName( 'body' )[ 0 ].appendChild( audio );
                        // audio.src = url;
                        break;

                    // ------------------------------------------------------------- CSS
                    case 'css':

                        load( url, function( err, res ) {
                            // create stylesheet
                            var style = document.createElement( 'style' );
                            style.type = 'text/css';
                            style.media = 'screen';

                            // add css rules
                            if ( style.styleSheet )
                                style.styleSheet.cssText = res.response;
                            else style.appendChild(
                                document.createTextNode( res.response )
                            );

                            // append to the header
                            document.getElementsByTagName( 'head' )[ 0 ].appendChild( style );
                            // callback
                            callback( null, style );
                        });
                        break;

                    // ------------------------------------------------------------- Html
                    case 'html':

                        load( url, function( err, res ) {
                            callback( null, Dom.html( res.response ) || [] );
                        });
                        break;

                    // ------------------------------------------------------------- Text
                    case 'text':

                        load( url, callback );
                        break;

                    // ------------------------------------------------------------- Json
                    case 'json':

                        load( url, function( err, res ) {
                            var json;
                            // transport errors
                            if ( err || !res || !res.response )
                                return callback( err || new Error( 'Empty JSON' ));
                            // parsing
                            try {
                                // remove comments and parse
                                json = JSON.parse( res.response.replace( /\/\*(.|[\r\n])*?\*\//gm, '' ));
                            }
                            catch ( err ) {
                                return callback( new Error( 'Error parse JSON' ));
                            }
                            callback( null, json );
                        });
                        break;

                    // -------------------------------------------------------------
                    default:
                        _callback( new Error( 'Unknown asset type: ' + config.type + ' (' + name + ')' ));
                }
            },

            get: function( name ) {
                return this._storage[ name ];
            }

        });


        // API
        // ---

        var api = Namespace(
            function( space ) {
                return new Assets();
            });
        api.Assets = Assets;
        return api;

    });


