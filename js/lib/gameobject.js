/**
 *
 * Game Object
 * --------------------
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'gameobject',
    [
        'lib/class',
        'lib/scene/layer',
        'lib/config',
        'lib/assets',
        'lib/states',
        'lib/scene'
    ],
    function( Class, Layer, Config, Assets, States, Scene ) {

        'use strict';

        /**
         * Game Object Class
         */
        var GameObject = Class({

            extend: Layer,

            constructor: function( name, game, difinition ) {

                // optional args
                if ( arguments.length == 1
                    && 'string' != typeof name )
                    difinition = name,
                    name = undefined;
                if ( 'string' != typeof game )
                    difinition = game,
                    game = undefined;

                // superclass
                this.super( name, {
                    visible: difinition.visible
                });

                // properties
                this.game = game;

                // initialize game object
                if ( game ) {
                    this.config = Config( game );
                    this.assets = Assets( game );
                    this.scene = Scene( game );
                }
                this.states = new States.Class();
            },

            _game: undefined,
            config: undefined,
            assets: undefined,
            states: undefined,
            scene: undefined,



        });



        // API
        // ---

        return GameObject;

    });