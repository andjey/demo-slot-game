/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Animation
 * ---------
 *
 * Events:
 *
 *  animation.begin()
 *  animation.end()
 *
 *
 *
 * TODO easing
 */

define( 'animate',
    [
        'lib/class',
        'lib/emitter',
        'lib/objects',
        'lib/polyfills'
    ],
    function( Class, Emitter, Objects ) {

        'use strict';

        var
            // Easing functions
            easings = {

            },

            // Animator Class
            Animate = Class({

            extend: Emitter,

            constructor: function() {
                this.super();
                // properties
                this.queue = [];
                this.in_pause = false;
                this.in_process = false;
            },


            // Properties
            // ----------

            // animation queue
            queue: null,
            // flags
            in_pause: null,
            in_process: null,
            // timestamp when pause start
            _pauseTimestamp: null,

            // Playback control
            // ----------------

            start: function() {

                // first time only
                if ( !this.in_process ) {
                    this.in_process = true;
                    // render the scene
                    this.emitAll( 'refresh' );
                    // TODO: emit only when animation really started
                }

                this.in_process = true;
                return this;
            },

            /**
             * Stop render and remove all animations
             * @param {boolean} [finish] - reset animation properties to final values
             * @returns {Animate}
             * @private
             */
            stop: function( finish ) {
                var anim;
                for ( var i = 0; i < this.queue.length; i++ ) {
                    anim = this.queue[ i ];

                    // finish if need (brake otherwise)
                    if ( finish ) {
                        var result = 0;
                        // calculate value
                        if ( 'distance' == anim.type )
                            result = anim.value + anim.distance;
                        if ( 'dynamic' == anim.type )
                            result = anim.dynamic( 1, anim.value );
                        // set final value
                        anim.object[ anim.property ] = result;
                    }
                    // remove from queue
                    this.queue.splice( i--, 1 );
                    // execute callback
                    if ( anim.callback ) anim.callback();
                }
                // reset flags
                this.in_pause = false;
                this.in_process = false;
                // chain
                return this;
            },

            /**
             * Pause all animations
             * @param [duration]
             * @returns {Animate}
             */
            pause: function( duration ) {

                // already stopped
                if ( !this.in_process ) return this;
                // already paused
                if ( this.in_pause ) return this;
                this.in_pause = true;

                // remember current time
                this._pauseTimestamp = + new Date;
                return this;
            },

            /**
             * Resume all animations
             * @returns {Animate}
             */
            resume: function() {

                // not in pause
                if ( !this.in_pause ) return this;
                // reset flag
                this.in_pause = false;
                // calculate delay time
                var delay = + new Date - this._pauseTimestamp;
                // increase timestamp of each active animation
                this.queue.forEach(
                    function( anim ) {
                        anim.timestamp += delay;
                    });
                // start animations
                if ( this.queue.length )
                    this.emitAll( 'refresh' );
                // chain
                return this;
            },

            // links

            stopAll: function() { this.stop.apply( this, arguments ); },
            pauseAll: function() { this.pause.apply( this, arguments ); },
            resumeAll: function() { this.resume.apply( this, arguments ); },


            // Animations
            // ----------

            /**
             * Process the frame of animations
             * @returns {boolean} continue animation
             * @private
             */
            renderAnimationFrame: function() {
                var
                    i,
                    anim,
                    result,
                    progress;

                // nothing to animate
                if ( !this.queue.length ) return false;

                // step each of animation in the queue
                for ( i = 0; i < this.queue.length; i++ ) {

                    anim = this.queue[ i ];

                    // wait for start
                    if ( anim.start + anim.timestamp > + new Date )
                        continue;

                    // ---->
                    // current progress
                    // `end` will recalculate when the pause released
                    progress = Math.min(( anim.duration - ( anim.start + anim.end + anim.timestamp - new Date )) / anim.duration, 1 );

                    // calculate new value
                    if ( 'distance' == anim.type )
                        progress = !anim.easing ? progress
                            : anim.easing( progress, anim.distance ),
                        result = anim.value + ( anim.distance * progress );
                    if ( 'dynamic' == anim.type ) {
                        result = anim.dynamic( progress, anim.value );
                    }

                    // update property value
                    anim.object[ anim.property ] = anim.pattern
                        ? anim.pattern.replace( '#', result )
                        : result;
                    // <-----

                    // stop on finish
                    if ( progress == 1 ) {
                        // repeat
                        if ( 0 === anim.repeats || -- anim.repeats ) {
                            // reset timestamp
                            anim.timestamp = + new Date;
                            // reset delay on start
                            anim.start = anim.after || anim.start || 0;
                            // TODO: stop render until animation will started
                        } else {
                            // remove from queue
                            this.queue.splice( i--, 1 );
                            // execute callback
                            if ( anim.callback ) anim.callback();
                        }
                    }
                }

                // last time only
                if ( !this.queue.length )
                    return this.in_process = false;
                // success
                return true;
            },


            // Configurators
            // -------------

            /**
             * Animate object property
             *
             * Params:
             *
             *  silent {boolean} - will not cast events
             *  delay {number} - delay before start (in seconds)
             *  repeat {number} - repeat times (0 = infinity, default = 1)
             *
             * @param {string} property Property name ( can be dot-notated )
             * @param {number} value
             * @param {string} [pattern] Substitute the value to the pattern string (like 2 -> '#px' -> '2px')
             * @param {number} duration duration in seconds
             * @param {string|function|null} [easing] easing
             * @param {object} [params] easing
             * @param {function} [callback]
             * @returns {Animate}
             */
            animate: function( property, value, pattern, duration, easing, params, callback ) {

                // optional arguments
                if ( + pattern )
                    callback = params,
                    params = easing,
                    easing = duration,
                    duration = pattern,
                    pattern = undefined;
                if ( 'object' == typeof easing )
                    callback = params,
                    params = easing,
                    easing = undefined;
                if ( 'function' == typeof params )
                    callback = params,
                    params = undefined;
                if ( !params && !callback )
                    callback = easing,
                    easing = undefined;

                var
                    // default arguments
                    args = [].slice.call( arguments ),
                    object = this, // 'object' == typeof object ? object : false,
                    property = 'string' == typeof property ? property : false,
                    is_number = !isNaN( + value ),
                    is_function = 'function' == typeof value,
                    duration = + duration,
                    params = params || {},
                    callback = 'function' == typeof callback ? callback : false,

                    // locals
                    self = this,
                    // repeats
                    repeats = undefined !== params.repeats ? params.repeats : 1,
                    // dot notation
                    dotted = property && !!~ property.indexOf( '.' ),
                    dotted_names,
                    // animation
                    config;

                // prepare the dot-notated property name
                if ( dotted ) {
                    dotted_names = property.split( '.' );
                    property = dotted_names.pop();
                    object = Objects.dotted( object, dotted_names.join( '.' ));
                }
                // get easing by name
                if ( 'string' == typeof easing )
                    easing = easings[ easing ];

                // checking
                if ( !object || !property || ( !is_number && !is_function ) || isNaN( duration ))
                    return self;

                // configure animation
                config = {
                    uid: Math.random().toString( 16 ).substr( 2 ),
                    timestamp: + new Date,
                    start: + params.delay || 0,
                    end: duration,
                    after: + params.delay_after || 0,
                    duration: duration,
                    object: object,
                    property: property,
                    pattern: pattern || '',
                    value: + object[ property ] || 0,  // number
                    callback: callback,
                    repeats: repeats  // 0 = infinity
                };

                // kind of animation
                // distance value
                if ( is_number )
                    config.type = 'distance',
                    config.distance = value - config.value,  // number
                    config.easing = easing && 'function' != easing ? easing : false;  // function
                // dynamic value
                if ( is_function )
                    config.type = 'dynamic',
                    config.dynamic = value;  // function


                // override previously animation of this property
                self.queue
                    .some( function( anim, index ) {
                        if ( object === anim.object
                            && property === anim.property )
                        {
                            // real property value
                            config.value = anim.value;
                            // override animation
                            return ( self.queue.splice( index, 1, config )), true;
                        }
                    })
                ||  // or
                // enqueue this animation
                self.queue.push( config );
                // animate it
                self.start();
                // chain
                return self;
            },

            /**
             * Setup complex animation
             *
             * Config:
             *
             *  value {number|function}
             *  duration {int}
             *  delay {int}
             *  easing {string|function}
             *  repeat
             *
             * Example:
             *
             *      x: { value: 100, duration: 50, delay: 20 },
             *      y: { value: 0, duration: 100, easing: 'wave' }
             *
             * @param {object} definition
             * @param {number} [duration]
             * @param {number|boolean} [repeats]
             * @param {function} [callback]
             * @returns {Animate}
             */
            animations: function( definition, duration, repeats, callback ) {
                // optional arguments
                if ( 'function' == typeof duration )
                    callback = duration,
                    repeats = null,
                    duration = null;
                if ( 'function' == typeof repeats )
                    callback = repeats,
                    repeats = duration,
                    duration = null;
                var
                    // args
                    definition = 'Object' == Objects.type( definition ) ? definition : false,
                    properties = definition && Object.keys( definition ),
                    callback = 'function' == typeof callback ? callback : false,
                    // locals
                    self = this,
                    // wait when completed each of animations
                    counter = properties && properties.length;

                if ( properties ) {
                    properties.forEach( function( prop_name ) {
                        var config = definition[ prop_name ] || {};
                        // begin animation
                        self.animate(
                            prop_name,
                            config.value,
                            config.duration || duration || null,
                            config.easing,
                            {
                                silent: true,
                                delay: config.delay,
                                repeats: undefined !== config.repeats ? config.repeats : repeats,
                                delay_after: config.delay_after
                            } ,
                            // on complete callback
                            function() { if (! --counter ) callback && callback(); });
                    });
                }
                return this;
            }
        });


        // API
        // ---

        return Animate;

    });
