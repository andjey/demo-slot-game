/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene: Layer
 * ------------
 *
 * Events:
 *
 *  remove( layer )
 *  refresh - force scene render
 *
 */

define( 'layer',
    [
        'lib/class',
        'lib/animate',
        'lib/objects',
        'lib/assets',

        // components
        'lib/scene/component/sprite',
        'lib/scene/component/text',
        'lib/scene/component/cover',
        'lib/scene/component/buffer'
    ],
    function( Class, Animate, Objects, Assets, Sprite, Text, Cover, Buffer ) {

        'use strict';

        var
            /**
             * Scene Layer Class
             * -----------------
             *
             * @type {Layer}
             */
            Layer = Class({

                extend: Animate,

                constructor: function( name, options ) {

                    // optional
                    if ( 'string' != typeof name )
                        options = name,
                        name = undefined;
                    var options = options || {};

                    // super class
                    this.super();

                    // init
                    this.name = '' + ( name || '' ) || null;
                    this.parents = [];
                    this.children = [];
                    this.named_children = {};

                    this.x = 0;
                    this.y = 0;
                    this.visible = undefined === options.visible
                        ? true : !! options.visible;
                },

                // properties

                parents: null,
                name: null,
                children: null,
                named_children: null,

                x: null,
                y: null,
                visible: null,


                // Logic
                // -----

                remove: function() {
                    // remove children
                    this.removeAll();
                    // remove layer
                    if ( this.parents )
                        this.parents.forEach( function( parent) {
                            parent.removeChild( this );
                        })
                    // events
                    this.emit( 'remove', this );
                    this.emitAll( 'refresh' );
                },


                // Components
                // ----------

                /**
                 * Add sub-layer
                 * @param {Layer|string} layer - name or Layer object
                 * @param {object} [params] - parameters
                 * @returns {Layer|boolean}
                 */
                layer: function( layer, params ) {
                    // create new layer
                    if ( 'string' == typeof layer )
                        return this.add( layer, 'layer', params ), this.get( layer );
                    else
                    // add already created layer
                    if ( layer instanceof Layer ) {
                        this.add( layer.name, 'layer', layer, params );
                        // start rendering of active animations
                        var self = this;
                        this._applyChildren( function( child ) {
                            if ( child.queue.length )
                                self.emitAll( 'animation.begin', child.queue.length );
                        })
                        return layer;
                    }
                    else
                    // something went wrong
                        return console.warn( 'Bad Layer!' ), this;
                },

                /**
                 * Add children to the layer
                 * @param {string} [name] for getter
                 * @param {string} kind of object (sprite|text|cover)
                 * @param {*} source asset (sprite, text)
                 * @param {object} [params]
                 * @returns {Layer}
                 */
                add: function( name, kind, source, params ) {
                    // optional args
                    if ( arguments.length < 3 )
                        params = source,
                        source = kind,
                        kind = name,
                        name = undefined;
                    if ( 'string' != typeof kind )
                        params = source,
                        source = kind,
                        kind = undefined;
                    var
                        name = '' + ( name || '' ),
                        kind = '' + ( kind || '' ),
                        type,
                        child,
                        self = this;

                    // create child in given kind
                    type = types[ kind ];
                    if ( !type ) return ( console.warn( 'Bad Kind of the child!' )), this;

                    if ( source instanceof type ) child = source;

                    if ( !child )
                        try { child = new type( source, params ); }
                        catch( err ) { }

                    // checking
                    if ( !child ) {
                        debugger
                        return ( console.warn( 'No Child was created!' )), this;
                    }

                    // keep child
                    child.parents.push( self );
                    self.children.push( child );
                    if ( name ) self.named_children[ name ] = child;

                    // event
                    this.emitAll( 'refresh' );
                    // chain
                    return self;
                },

                /**
                 * Get layer child by source
                 * Name can be in dot-notation like 'layer1.layer2.sprite3'
                 * @param {string|*} source - Name or source of the child to find
                 * @returns {*|undefined}
                 */
                get: function( source ) {
                    var
                        name = 'string' == typeof source ? source : false,
                        child, i,
                        // dot notation
                        names = name && name.split( '.' ),
                        target = this;

                    // name in dot-notation
                    if ( names.length > 1 ) {
                        while ( i = names.shift() )
                            if ( target instanceof Layer )
                                target = target.named_children[ i ];
                        return target;
                    }
                    // by name
                    if ( name ) return this.named_children[ name ];

                    // strict comparison
                    for ( i in this.children ) {
                        child = this.children[ i ];
                        if ( source === child.source )
                            return child;
                    }
                },

                /**
                 * Remove child from the layer
                 * @param {string} source - Name or source or child_object to find and remove the child
                 * @returns {Layer}
                 */
                removeChild: function( source ) {
                    var self = this,
                        name = 'string' == typeof source ? source : false,
                        child;

                    // remove from named hash
                    if ( name ) {
                        child = self.named_children[ name ];
                        source = child && child.source;
                        delete self.named_children[ name ];
                        for ( var i = 0; i < self.children.length; i ++ )
                            if ( self.children[ i ] === child )
                                return self.children.splice( i --, 1 ), self;
                    }
                    if ( !source ) return self;

                    // remove from the array
                    for ( var i = 0; i < self.children.length; i ++ )
                        if ( source === child
                            || source === child.source )
                        {
                            // remove from array
                            var removed = self.children.splice( i --, 1 );
                            // remove from named
                            if ( removed.name )
                                delete self.named_children[ removed.name ];
                            // remove itself
                            child.remove();
                            // event
                            self.emitAll( 'refresh' );
                            // found, break
                            return self;
                        }
                    // chain
                    return self;
                },


                // Visual
                // ------

                /**
                 * Show layer content
                 * @param {boolean} [silent]
                 * @returns {Layer}
                 */
                show: function( silent ) {
                    this.visible = true;
                    if ( !silent ) {
                        this.emit( 'show' );
                        this.emitAll( 'refresh' );
                    }
                    return this;
                },

                /**
                 * Hide layer content
                 * @param {boolean} [silent]
                 * @returns {Layer}
                 */
                hide: function( silent ) {
                    this.visible = false;
                    if ( !silent ) {
                        this.emit( 'hide' );
                        this.emitAll( 'refresh' );
                    }
                    return this;
                },

                /**
                 * Move layer content
                 * @param {number|null} ofs_x
                 * @param {number|null} ofs_y
                 * @param {number} duration
                 * @param {function} [callback]
                 */
                move: function( x, y, duration, callback ) {
                    this.animations({
                        x: { value: + x || 0, duration: duration, repeats: 1 },
                        y: { value: + y || 0, duration: duration, repeats: 1 }
                    }, callback );
                    return this;
                },

                scroll: function( ofs_x, ofs_y, duration, callback ) {
                    return this.move( this.x + ofs_x, this.y + ofs_y, duration, callback );
                },

                /**
                 * Relocate all layer children
                 * @param {number} x
                 * @param {number} y
                 */
                position: function( x, y ) {
                    this.x = x;
                    this.y = y;
                    this.emitAll( 'refresh' );
                    return this;
                },


                // Child Control
                // -------------

                _applyChildren: function( iterator ) {
                    this.children.forEach( function( child ) {
                        iterator( child );
                        if ( child instanceof Layer )
                            child._applyChildren( iterator );
                    });
                    return this;
                },

                // Visual

                showAll: function() {
                    this._applyChildren(
                        function( child ) {
                            child.show( true );
                        });
                    this.emitAll( 'refresh' );
                    return this;
                },

                hideAll: function() {
                    this._applyChildren(
                        function( child ) {
                            child.hide( true );
                        });
                    this.emitAll( 'refresh' );
                    return this;
                },

                // Animations

                stopAll: function() {
                    return this._applyChildren(
                        function( child ) {
                            child.stop();
                        });
                },

                pauseAll: function() {
                    return this._applyChildren(
                        function( child ) {
                            child.pause();
                        });
                },

                resumeAll: function() {
                    return this._applyChildren(
                        function( child ) {
                            child.resume();
                        });
                },

                // Removing

                removeAll: function() {
                    // remove children
                    this.children
                        .forEach( function( child ) {
                            child.remove();
                        });
                    this.children = [];
                    this.emit( 'refresh' );
                }

            }),

            // Available Component types
            types = {
                layer: Layer,
                sprite: Sprite,
                text: Text,
                cover: Cover,
                buffer: Buffer
            };


        // API
        // ---

        return Layer;

    });