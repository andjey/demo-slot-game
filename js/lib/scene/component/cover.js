/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component: Sprite
 * -----------------------
 *
 */

define( 'cover',
    [
        'lib/class',
        'lib/scene/component'
    ],
    function( Class, SceneComponent ) {

        return Class({

            extend: SceneComponent,

            /**
             * Options:
             *
             *  image {Image} - image to fill
             *  color {string} - css color to fill, like '556270'
             *
             * @param options
             */
            constructor: function( options ) {
                // init super class
                this.super();

                // options
                options = options || {};

                // check image
                if ( options.image )
                    if ( options.image instanceof Image )
                        this.source = options.image;
                    else throw 'Bad image';
                if ( options.color )
                    this.color = options.color;

                // box (can be out of screen for scrolling)
                this.x = !isNaN( + options.x ) ? + options.x : ( this.source && - this.source.width ) || 0;
                this.y = !isNaN( + options.y  ) ? + options.y : ( this.source && - this.source.height ) || 0;
                this.width = + options.width || false;
                this.height = + options.height || false;
            },

            source: null,
            color: null,

            /**
             * Main render function:
             * Fill with image or color
             * @param canvas
             */
            render: function( canvas ) {
                // use buffer
                if ( !this.buffer
                    || this.buffer.width != this.width
                    || this.buffer.height != this.height )
                    this._renderToBuffer( canvas.width, canvas.height );

                // show buffer (out of screen to scroll if need)
                canvas.drawImage( this.buffer, this.x, this.y );
            },

            /**
             * Render to the buffer
             * @private
             */
            _renderToBuffer: function( canvas_width, canvas_height ) {
                var
                    // calculations
                    source_width = this.source ? this.source.width : 0,
                    source_height = this.source ? this.source.height : 0,
                    // out of screen (for the scrolling)
                    w = this.width || canvas_width + source_width * 2,
                    h = this.height || canvas_height + source_height * 2,
                    // canvas
                    buffer = this._createBuffer( w, h );

                // clear buffer
                buffer.clear();

                // fill area with color
                if ( this.color && !this.source ) {
                    buffer.fillStyle = this.color;
                    buffer.fillRect( 0, 0, w, h );
                }
                // fill area with image
                if ( this.source )
                    buffer.fillImage( this.source, 0, 0, w, h );
            }

        });

    });