/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component: Sprite
 * -----------------------
 *
 */

define( 'buffer',
    [
        'lib/class',
        'lib/scene/component',
        'lib/canvas'
    ],
    function( Class, SceneComponent, Canvas ) {

        return Class({

            extend: SceneComponent,

            constructor: function( options ) {
                var options = options || {};

                // init super class
                this.super();

                // init
                this.source = null;
                this.visible = undefined === options.visible
                    ? true : !! options.visible;

                // box
                this.x = + options.x || 0;
                this.y = + options.y || 0;
                this.width = + options.width || 0;
                this.height = + options.height || 0;

                this._createBuffer( this.width, this.height );
            },

            source: null,

            /**
             * Main render function
             * @param canvas
             */
            render: function( canvas ) {
                // show image
                canvas.drawImage( this.buffer, this.x, this.y, this.width, this.height );
            }

        });

    });