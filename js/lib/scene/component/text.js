/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component: Text
 * ---------------------
 *
 */

define( 'text',
    [
        'lib/class',
        'lib/scene/component',
        'lib/canvas'
    ],
    function( Class, SceneComponent, Canvas ) {

        return Class({

            extend: SceneComponent,

            constructor: function( text, options ) {
                var options = options || {};

                // init super class
                this.super();
                // check text
                if ( 'string' != typeof text ) throw 'Bad text';

                // init
                this.source = text;
                this.visible = undefined === options.visible
                    ? true : !! options.visible;

                // font
                this.color = options.color || '#000';  // black
                this.font = options.font || 'Helvetica, Sans-serif';
                this.size = options.size || '14px';
                this.nowrap = undefined === options.nowrap ? false : !! options.nowrap;

                // box
                this.x = + options.x || 0;
                this.y = + options.y || 0;
                this.width = + options.width || false;
                this.max_width = + options.max_width || false;
                this.line_height = + options.line_height || false;
                this.baseline = + options.baseline || false;
                this.align = options.align || 'left';

                // prerender text to the buffer
                this._createBuffer();
                this._renderToBuffer();
            },

            source: null,
            color: null,
            font: null,
            size: null,
            nowrap: null,
            width: null,
            line_height: null,
            baseline: null,
            align: null,

            /**
             * Change text string
             * @param {string} string
             * @returns {Text}
             */
            text: function( string ) {
                var string = 'string' == typeof string ? string : false;
                if ( string ) this.source = string;
                // render text
                this._renderToBuffer();
                // refresh scene
                this.emitAll( 'refresh' );
                return this;
            },

            /**
             * Prerender text to the buffer
             * @private
             */
            _renderToBuffer: function() {
                var canvas = this.buffer;
                // clear buffer
                canvas.clear();
                // prepare text
                canvas.font = [ this.size, this.font ].join( ' ' );
                canvas.fillStyle = this.color;
                canvas.textAlign = this.align;
                // show text
                canvas.wrapText(
                    this.source, 0, 0,
                    {
                        max_width: this.max_width,
                        line_height: this.line_height,
                        width: this.width,
                        baseline: this.baseline,
                        resize: true,
                        nowrap: this.nowrap,
                        canvas: canvas,
                        context: canvas.context
                    }
                );
            }

        });

    });