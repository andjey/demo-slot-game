/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component: Sprite
 * -----------------------
 *
 */

define( 'sprite',
    [
        'lib/class',
        'lib/scene/component'
    ],
    function( Class, SceneComponent ) {

        return Class({

            extend: SceneComponent,

            constructor: function( image, options ) {
                var options = options || {};

                // init super class
                this.super( options );

                // check image
                if (!( image instanceof Image )) throw 'Bad image';

                // init
                this.source = image;
                this.visible = undefined === options.visible
                    ? true : !! options.visible;

                // box
                this.x = + options.x || 0;
                this.y = + options.y || 0;
                this.width = + options.width || image.width;
                this.height = + options.height || image.height;

                // events
                this.alpha_level = undefined === options.alpha_level ? 0 : options.alpha_level >> 0;  // int

                // debug
                this.uid = Math.random().toString( 16 ).substr( 2 );
            },

            source: null,
            alpha_level: null,  // alpha level which recognized as transparent on mouse events

            /**
             * Main render function
             * @param canvas
             */
            render: function( canvas ) {
                // show image
                canvas.drawImage(
                    // image may be buffered to apply effects
                    this.buffer || this.source,
                    this.x, this.y,
                    this.width, this.height
                );
            },

            /**
             * Detect mouse intersection
             * Check alpha level of pixel at the mouse position
             * @param x
             * @param y
             */
            intersect: function( x, y ) {
                // detect intersection
                if ( !this.alpha_level ) return true;
                if ( !this.buffer ) this._renderToBuffer();
                var pixel = this.buffer.getImageData( x, y, 1, 1 ).data;
                return pixel[ 3 ] >= this.alpha_level;
            },

            /**
             * Render image to the buffer
             * (for example to apply effects to the image)
             * @private
             */
            _renderToBuffer: function() {
                // buffer image
                this._createBuffer( this.width, this.height );
                this.buffer.clear()
                    .drawImage( this.source, 0, 0, this.width, this.height );
            }

        });

    });