/**
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 *
 * Scene Component
 * ---------------
 *
 * Events:
 *
 *  refresh - force scene render
 *
 */

define( 'component',
    [
        'lib/class',
        'lib/animate',
        'lib/canvas'
    ],
    function( Class, Animate, Canvas ) {

        return Class({

            extend: Animate,

            constructor: function( options ) {
                var options = options || {};

                // super class
                this.super();

                // setup
                this.parents = [];
                this.visible = undefined === options.visible ? true : !! options.visible;  // bool
                this.x = null;
                this.y = null;
                this.alpha = null;
                this.width = null;
                this.height = null;

                this.use_buffer = undefined === options.use_buffer ? false : !! options.use_buffer;  // bool
                this.buffer = null;
            },

            // properties

            parents: null,
            visible: null,
            x: null,
            y: null,
            width: null,
            height: null,
            alpha: null,  // level of transparency

            // Render buffer
            // -------------

            use_buffer: null,
            buffer: null,

            /**
             * Create backbuffer canvas
             * @param {number} [width=0]
             * @param {number} [height=0]
             * @private
             */
            _createBuffer: function( width, height ) {
                if ( this.buffer ) {
                    // resize if need
                    if ( height != this.buffer.canvas.height )
                        this.buffer.setHeight( height );
                    if ( width != this.buffer.canvas.width )
                        this.buffer.setHeight( width );
                    return this.buffer;
                }
                // create buffer
                this.buffer = new Canvas(
                    + width || 0,
                    + height || 0,
                    { use_buffer: false }
                );
                return this.buffer;
            },

            /**
             * Render source to the buffer
             * OVERRIDE
             * @private
             */
            _renderToBuffer: function() {
                // override only
            },

            /**
             * Main render function
             * OVERRIDE
             * @param canvas
             */
            render: function( canvas ) {
                if ( canvas && this.buffer )
                    canvas.drawImage( this.buffer, + this.x || 0, + this.y || 0 );
            },


            // Component
            // ---------

            remove: function() {
                // stop animations
                this.stop();
                // hide
                this.visible = false;
                // remove events
                this.off();
            },


            // Visual
            // ------

            show: function( silent ) {
                this.visible = true;
                if ( !silent ) this.emitAll( 'refresh' );
                return this;
            },

            hide: function( silent ) {
                this.visible = false;
                if ( !silent ) this.emitAll( 'refresh' );
                return this;
            },


            // Animations
            // ----------

            move: function( x, y, duration, repeats, callback ) {

                // optional args
                if ( 'function' == typeof repeats )
                    callback = repeats,
                    repeats = undefined;
                var
                    x = + x,
                    y = + y,
                    duration = + duration || false,
                    repeats = !isNaN( + repeats ) ? repeats : 1,  // only one time
                    props = {};

                // checking
                if ( false === duration) return this;
                if ( !isNaN( x ) && null != x ) props.x = { value: x, duration: duration, repeats: repeats };
                if ( !isNaN( y ) && null != y ) props.y = { value: y, duration: duration, repeats: repeats };
                // animate
                this.animations( props, callback );
                // chain
                return this;
            },

            slide: function( ofs_x, ofs_y, duration, callback ) {
                return this.move( this.x + ofs_x, this.y + ofs_y, duration, callback );
            },

            resize: function( width, height, duration, callback ) {
                var self = this;
                self.animations({
                    width: { value: width, duration: duration },
                    height: { value: height, duration: duration }
                }, callback );
                return self;  // chain
            },


//            // Events
//            // ------
//
//            /**
//             * Handle user interaction event
//             * @param type
//             * @param [params]
//             * @param listener
//             */
//            handle: function( type, params, listener ) {
//                this.on( 'ui.' + type, function( x, y, alpha ) {
//                    if ( alpha > params.alpha )
//                        listener( x, y );
//                });
//
////                // optional args
////                if ( 'function' == typeof params )
////                    listener = params,
////                    params = null;
////                // configure
////                params = params || {};
////                params.alpha = + params.alpha || .4;
////                // listen
////                this.on( type, function() {
////                })
//            },


            // Effects
            // -------

            /**
             *
             * @param {string} type Effect name
             * @param {number} [x=0]
             * @param {number} [y=0]
             * @param {number} [width]
             * @param {number} [height]
             */
            fx: function( type, x, y, width, height ) {
                // prepare buffer
                this._renderToBuffer();
                // apply effect
                this.buffer.effect( type,
                    + x || 0,
                    + y || 0,
                    + width || this.buffer.width,
                    + height || this.buffer.height,
                    { canvas: this.buffer }
                );
                // event
                this.emitAll( 'refresh' );
            },

            /**
             * Return source object to original state
             * Reset size, representation, etc..
             */
            origin: function() {
                // TODO reset size
                // reset effects
                this._renderToBuffer();
                this.emitAll( 'refresh' );
            }

        })

    });