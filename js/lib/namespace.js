/**
 *
 * Namespace
 * ---------
 *
 * fabric
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

define( 'namespace',
    [],
    function() {
        'use strict';

        function Namespaces( fabric ) {
            var spaces = {};
            return function init( name ) {

                // init new space
                if ( !spaces.hasOwnProperty( name ))
                    return spaces[ name ] = fabric.apply( this, arguments );
                // return exists space
                else return spaces[ name ];
            }
        }
        return Namespaces;
    });