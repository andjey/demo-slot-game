/**
 *
 * @author Andjey Guzhovskiy <me.the.ascii@gmail.com>
 */

var
    // core modules
    http = require( 'http' ),
    util = require( 'util' ),
    path = require( 'path' ),
    fs = require( 'fs' ),

    // server
    config = require( './config/server.json' ),
    app,

    // content types
    mime = {
        js: 'text/javascript',
        css: 'text/css',
        png: 'image/png',
        jpg: 'image/jpeg',
        jpeg: 'image/jpeg',
        gif: 'image/gif'
    };


http.createServer( function( req, res ) {

    switch( req.url ) {

    // Client messages
    // ---------------

    case '/message':
        var
            message = '';
        req.on( 'data', function( chunk ) {
            message += chunk;
        });
        req.on( 'end', function() {
            if ( config.log.message )
                // blue
                console.log( '\x1B[34m' + '[MESSAGE]' + '\x1B[39m', message );
        });
        res.end();
        break;

    // Client errors
    // -------------

    case '/error':
        var
            message = '',
            error = {};
        req.on( 'data', function( chunk ) {
            message += chunk;
        });
        req.on( 'end', function() {
            // parse error
            message.split( '&' ).forEach( function( v ) {
                var pair = v.split( '=' );
                error[ pair[ 0 ] ] = pair[ 1 ];
            });
            if ( config.log.error )
                // red
                console.log( '\x1B[31m' + '[ERROR]', error.message + '\x1B[39m',
                    '(' + error.url + ' ' + error.line + ':' + error.pos + ')' );
        });
        res.end();
        break;

    // Serve static
    // ------------

    default:
        var
            url = req.url == '/' ? '/index.html' : req.url,
            filepath = path.resolve( __dirname + url ),
            safe = __filename != filepath
                || !~filepath.indexOf( __dirname ),
            stream,
            type;

        // reject
        if ( !safe ) {
            res.statusCode = 403;
            return res.end();
        }
        // send file
        if ( config.log.static )
            if ( config.log.static )
                // green
                console.log( '\x1B[32m' + '[STATIC]' + '\x1B[39m', url );

        // mime header
        if ( type = mime[ path.extname( filepath ).substr( 1 )] )
            res.setHeader( 'Content-Type', type );

        // file transfer
        stream = fs.createReadStream( filepath );
        stream.on( 'error', function() {
            res.statusCode = 404;
            return res.end();
        });
        stream.on( 'end', function() {

        });
        stream.pipe( res );
        break;
    }

}).listen( config.port );
console.log( 'Server listen on port', config.port );